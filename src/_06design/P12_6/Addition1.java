package _06design.P12_6;

/**
 * This class generates an addition question for level1
 */
public class Addition1 extends Arithmetic{

    // Constructor generates a random question every time a new object is instantiated.
    public Addition1() {

        // Both operands are selected randomly such that the sum is lesser than 10.
        mOperand1 = (int)(Math.random() * ARITHMETIC_LIMIT);
        mOperand2 = (int)(Math.random() * (ARITHMETIC_LIMIT - mOperand1));
        mResult = mOperand1 + mOperand2;
    }

    // Method to return the question is string format.
    public String getQuestion() {
        return mOperand1 + " + " + mOperand2 + " = ";
    }
}
