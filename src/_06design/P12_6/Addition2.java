package _06design.P12_6;

/**
 * This class generates an addition question for level2
 */
public class Addition2 extends Arithmetic {


    public Addition2() {

        // Both operands are selected randomly but lesser than 10.
        mOperand1 = (int)(Math.random() * ARITHMETIC_LIMIT);
        mOperand2 = (int)(Math.random() * ARITHMETIC_LIMIT);
        mResult = mOperand1 + mOperand2;
    }

    // Method to return the question is string format.
    public String getQuestion() {
        return mOperand1 + " + " + mOperand2 + " = ";
    }

}