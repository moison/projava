package _06design.P12_6;

/**
 * Driver class for Arithmetic Teacher.
 */
public class Driver {
    public static void main(String[] args) {
        ArithmeticTeacher arithmeticTeacher = new ArithmeticTeacher();
        arithmeticTeacher.teachArithmetic();
    }
}
