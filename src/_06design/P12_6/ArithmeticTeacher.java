package _06design.P12_6;

import java.util.Scanner;

/**
 * This class has primary logic to combine the arithmetic questions and interact with user.
 */
public class ArithmeticTeacher {

    // Define member variables
    private int mLevel = 1;
    private int mScore = 0;
    private Arithmetic mArithmeticQuestion;
    private Scanner scnIn;
    private boolean mFirstQuestion = true;

    // Constructor to initialize member variable.
    public ArithmeticTeacher() {
        scnIn = new Scanner(System.in);
    }

    // Primary method to drive the quiz with various levels and user inputs.
    public void teachArithmetic() {

        // Define local variables
        boolean bGameOver = false;
        boolean bCorrectAnswer;
        int nAttempts;
        System.out.println("Welcome to the Arithmetic Game!");
        printLevel();

        // Logic to perform till all levels have been completed.
        do {
            nAttempts = 0;
            bCorrectAnswer = false;
            getQuestion();

            // Print question and give two attempts per question. Call methods to verify response and increment level.
            do {
                if (nAttempts == 1) {
                    System.out.println("Try again!");
                }
                printQuestion();
                nAttempts++;
                if(verifyResponse()) {
                    System.out.println("That's correct!");
                    mScore++;
                    bCorrectAnswer = true;
                } else {
                    System.out.println("That's incorrect!");
                }
            } while (nAttempts < 2 & !bCorrectAnswer);
            checkAdjustLevel();
            if (mLevel <= 3) {
                printLevel();
            } else {
                bGameOver = true;
                System.out.println("You've completed all levels. Good bye!");
            }
        } while (!bGameOver);
    }

    // Method to print current game level of user.
    private void printLevel() {
            System.out.println("\nCurrent Level: " + mLevel);
    }

    // Method to generate question based on current user level.
    private void getQuestion() {
        if (mFirstQuestion) {
            System.out.println("\nFirst Question!");
            mFirstQuestion = false;
        } else {
            System.out.println("\nNext Question!");
        }
        switch (mLevel) {
            case 1:
                mArithmeticQuestion = new Addition1();
                break;
            case 2:
                mArithmeticQuestion = new Addition2();
                break;
            case 3:
                mArithmeticQuestion = new Subtraction();
                break;
        }
    }

    // Method to print quiz question.
    private void printQuestion() {
        System.out.print(mArithmeticQuestion.getQuestion());
    }

    // Method to verify user response against the correct answer.
    private boolean verifyResponse() {
         return (mArithmeticQuestion.getResult() == scnIn.nextInt());
    }

    // Check user score and increment level.
    private void checkAdjustLevel() {
        if (mScore == 5) {
            mLevel++;
            mScore=0;
        }
    }

}
