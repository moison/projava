package _06design.P12_6;

/**
 * This class generates a subtraction question for level3
 */
public class Subtraction extends Arithmetic {

    public Subtraction() {
        // Assign first operand as any random single digit number
        mOperand1 = (int)(Math.random() * ARITHMETIC_LIMIT);

        // Since the result of the difference should be non-negative, we give the upper limit as Operand 1 and offset by 1 since Math.random will be lesser than 1
        mOperand2 = (int)(Math.random() * (mOperand1 + 1));

        mResult = mOperand1 - mOperand2;
    }

    // Method to return the question is string format.
    public String getQuestion() {
        return mOperand1 + " - " + mOperand2 + " = ";
    }
}
