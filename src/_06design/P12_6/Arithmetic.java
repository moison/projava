package _06design.P12_6;

/**
 * Abstract class to hold abstract methods and member variables.
 */
public abstract class Arithmetic {

    protected final int ARITHMETIC_LIMIT = 10;
    protected int mOperand1, mOperand2, mResult;

    abstract String getQuestion();

    public int getResult() {
        return mResult;
    }
}
