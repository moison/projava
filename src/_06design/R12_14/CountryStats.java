package _06design.R12_14;

/**
 * This class reads an input file with country information
 */
public class CountryStats {

    private Country mLargestByArea, mLargestByPopulation, mLargestByDensity;

    public void readFile() {

    }

    public void findLargestArea(){

    }

    public void findLargestPopulation(){

    }

    public void findLargestDensity() {

    }
}
