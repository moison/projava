package _06design.P12_8;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * This class simulates the working of a vending machine
 */
public class VendingMachine {

    // Define member variables to hold products, coins and transaction balance.
    private LinkedHashMap<Integer,Product> mProducts = new LinkedHashMap<>();
    private double mTransactionBalance = 0;
    private ArrayList<Coin> mSaleCoins, mTransactionCoins;

    // Constructor loads Hashmap with predefined list of products with price and quantity
    // It also initialized the array lists for sale coins and transaction coins.
    public VendingMachine() {
        mProducts.put(11,new Product("ABC1",1.99,3));
        mProducts.put(12,new Product("ABC2",1.99,3));
        mProducts.put(13,new Product("ABC3",1.99,3));
        mProducts.put(14,new Product("ABC4",1.99,3));
        mProducts.put(21,new Product("XYZ1",1.99,3));
        mProducts.put(22,new Product("XYZ2",1.99,3));
        mProducts.put(23,new Product("XYZ3",1.99,3));
        mProducts.put(24,new Product("XYZ4",1.99,3));
        mSaleCoins = new ArrayList<>();
        mTransactionCoins = new ArrayList<>();
    }

    // Method to retrieve the products
    public LinkedHashMap<Integer,Product> getProducts() {
        return mProducts;
    }

    // Method to add coins to buy a product. The transaction balance is updated when a coin is added.
    public void addCoins(Coin coinVend) {
        mTransactionCoins.add(coinVend);
        mTransactionBalance += coinVend.getCoinValue();
    }

    // Method to buy a product. Returns integer code to indicate whether buy action was successful or not.
    public int buyProduct(int nPosition) {

        // Return 1 if current transaction balance is lesser than value of product requested
        // Return 2 if product quantity is zero i.e. sold out
        // Return 0 if transaction was successful.
        if (mProducts.get(nPosition).getPrice() > mTransactionBalance) {
            return 1;
        } else if (mProducts.get(nPosition).getQuantity() == 0) {
            return 2;
        } else {
            // When transaction criteria is successful, move all transaction coins to sale coins and zero out transaction balance, and buy product.
            mSaleCoins.addAll(mTransactionCoins);
            this.clearTransactionCoins();
            mProducts.get(nPosition).buyProduct();
            return 0;
        }
    }

    // Method to return transaction coins in case of unsuccessful buy
    public ArrayList<Coin> returnTransactionCoins() {
        return mTransactionCoins;
    }

    // Method to clear transaction coins in case of successful buy
    public void clearTransactionCoins() {
        mTransactionCoins.clear();
        mTransactionBalance = 0;
    }

    // Method to retrieve sale coins by an operator
    public ArrayList<Coin> returnSaleCoins() {
        return mSaleCoins;
    }

    // Method to clear sale coins when operator has retrieved all coins.
    public void clearSaleCoins() {
        mSaleCoins.clear();
    }

    // Method to access current transaction balance.
    public double getTransactionBalance() {
        return mTransactionBalance;
    }

    // Method to add stock(products) to vending machine by operator.
    public void addStock(int nProductCode, int nQuantity) {
        mProducts.get(nProductCode).restockProduct(nQuantity);
    }

}
