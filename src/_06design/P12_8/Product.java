package _06design.P12_8;

/**
 * This class is designed to represent products in a vending machine.
 */
public class Product {

    // Define member variables.
    private String mProductName;
    private double mPrice;
    private int mQuantity;

    // Constructor to determine name of product, price and quantity
    public Product(String strProductName, double dPrice, int nQuantity) {
        mProductName = strProductName;
        mPrice = dPrice;
        mQuantity  = nQuantity;
    }

    // Method to retrieve price of product
    public double getPrice() {
        return mPrice;
    }

    // Method to retrieve name of product
    public String getProductName() {
        return mProductName;
    }

    // Method to retrieve quantity of product
    public int getQuantity() {
        return mQuantity;
    }

    // Method to decrement quantity of product when it is bought successfully
    public void buyProduct() {
        mQuantity--;
    }

    // Method to increment quantity of product when it is restocked
    public void restockProduct(int nQuantity) {
        mQuantity += nQuantity;
    }

}
