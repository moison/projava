package _06design.P12_8;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

/**
 * Driver program for Vending machine class. This class has all logic to interact with user.
 */
public class Driver {

    // Declare member variables
    private static final String OPERATOR_PASSCODE = "ABCDEF";
    private static Scanner mScnIn = new Scanner(System.in);
    private static VendingMachine mVendingMachine = new VendingMachine();
    private static NumberFormat numformatPrice = NumberFormat.getCurrencyInstance();

    public static void main(String[] args) {

        // Display menu to user, accept option and process input.
        int nOption = 0;
        do {
            System.out.println("\nVending Machine Main Menu");
            System.out.println("1 Display products");
            System.out.println("2 Add Coins");
            System.out.println("3 Buy Product");
            System.out.println("4 Restock product");
            System.out.println("5 Collect Sale Coins");
            System.out.println("6 Exit");
            System.out.print("Enter your option number: ");
            nOption = mScnIn.nextInt();

            // Call appropriate method based upon user option input.
            switch (nOption) {
                case 1:
                    displayProducts();
                    break;
                case 2:
                    addCoins();
                    break;
                case 3:
                    buyProduct();
                    break;
                case 4:
                    restockProduct();
                    break;
                case 5:
                    collectSaleCoins();
                    break;
                case 6:
                    System.out.println("Thanks for shopping!");
                    break;
            }
        } while (nOption!=6);

    }

    // Method to display all products available in the vending machine.
    private static void displayProducts() {
        LinkedHashMap<Integer,Product> currProducts = mVendingMachine.getProducts();
        System.out.println("Code\tProduct name\tPrice");
        for (int nPosition : currProducts.keySet()) {
            System.out.println(nPosition + "\t\t" + currProducts.get(nPosition).getProductName() +
                    "\t\t\t" + numformatPrice.format(currProducts.get(nPosition).getPrice()));
        }
    }

    // Method to add coins to the vending machine.
    private static void addCoins() {
        System.out.println("Enter coins(n,d,q) separated by space: ");
        mScnIn.nextLine();
        String[] strCoins = mScnIn.nextLine().split(" ");
        for (String strCoin : strCoins ) {
            if (strCoin.equalsIgnoreCase("n") || strCoin.equalsIgnoreCase("d") || strCoin.equalsIgnoreCase("q")) {
                mVendingMachine.addCoins(new Coin(strCoin));
            } else {
                System.out.println("Invalid coin " + strCoin);
            }
        }
        System.out.println("Your current balance is :" + numformatPrice.format(mVendingMachine.getTransactionBalance()));
    }

    // Method to buy product and decipher return code from the vending machine.
    private static void buyProduct() {
        System.out.print("Enter the product code: ");
        int nSaleReturnCode = mVendingMachine.buyProduct(mScnIn.nextInt());
        switch (nSaleReturnCode){
            case 0:
                System.out.println("Transaction successful! Product dispensed." );
                break;
            case 1:
                System.out.println("Insufficient funds");
                returnCoins();
                break;
            case 2:
                System.out.println("Product out of stock");
                returnCoins();
                break;
        }
    }

    // Method to return coins to user in case of unsuccessful transaction.
    private static void returnCoins() {
        ArrayList<Coin> coinsToReturn = mVendingMachine.returnTransactionCoins();
        System.out.println("Here are your coins back: ");
        for (Coin coin : coinsToReturn ) {
            System.out.print(coin.getCoinType() + " ");
        }
        mVendingMachine.clearTransactionCoins();
    }

    // Method for operator to collect sale coins
    private static void collectSaleCoins() {
        System.out.print("Enter your passcode: ");
        mScnIn.nextLine();
        if (mScnIn.nextLine().equals(OPERATOR_PASSCODE)) {
            ArrayList<Coin> coinsForOperator = mVendingMachine.returnSaleCoins();
            System.out.println("Here are the coins from the register: ");
            for (Coin coin : coinsForOperator ) {
                System.out.print(coin.getCoinType() + " ");
            }
            System.out.println("");
            mVendingMachine.clearSaleCoins();
        } else {
            System.out.println("Invalid passcode.");
        }
    }

    // Method for products to be restocked by the operator.
    private static void restockProduct() {
        System.out.print("Enter product code of the product to be restocked: ");
        int nProductCode = mScnIn.nextInt();
        System.out.print("Enter quantity: ");
        int nQuantity = mScnIn.nextInt();
        mVendingMachine.addStock(nProductCode,nQuantity);
    }
}
