package _06design.P12_8;

/**
 * This class is designed to represent a coin used in a vending machine. It has properties such as coin type and coin value.
 */
public class Coin {

    // Define member variables to hold coin type and coin value.
    private String mCoinType;
    private double mCoinValue;

    // Constructor to receive coin type and determine coin value.
    public Coin (String strCoinType) {
        mCoinType = strCoinType;
        switch (strCoinType.toUpperCase()) {
            case "N":
                mCoinValue = 0.05;
                break;
            case "D":
                mCoinValue = 0.10;
                break;
            case "Q":
                mCoinValue = 0.25;
                break;
        }

    }

    // Method to return coin value
    public double getCoinValue() {
        return mCoinValue;
    }

    // Method to return coin type
    public String getCoinType() {
        return mCoinType;
    }
}
