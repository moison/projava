package _02arrays;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by John on 4/9/2016.
 * This program generates a histogram based on the caption and value provided by user.
 */
public class P6_23 {
    public static void main(String[] args) {
        final int nMAX_BAR_LENGTH = 40;
        Scanner in = new Scanner(System.in);
        ArrayList<String> strCaptions = new ArrayList<>();
        ArrayList<Integer> nValues = new ArrayList<>();
        System.out.println("Please enter caption and value per line separated by space. Type 'Exit' to proceed with histogram generation.");

        int nMaxValue = 0;
        int nMaxCaptionLength = 0;
        int nCurrValue;

        // Capture input from console
        while (in.hasNextLine()) {

            // Split input values by space
            String strInpValues[] = in.nextLine().split(" ");

            //Check if input value was Exit, then break loop.
            if (strInpValues[0].equalsIgnoreCase("Exit") || strInpValues[0].equals("")) {
                break;
            } else {
                // Add the caption and value to array list while checking for maximum value
                strCaptions.add((strInpValues[0]));
                nCurrValue=Integer.parseInt(strInpValues[1]);
                nValues.add(nCurrValue);
                if (strInpValues[0].length() > nMaxCaptionLength) {
                    nMaxCaptionLength = strInpValues[0].length();
                }
                if (nCurrValue > nMaxValue) {
                    nMaxValue = nCurrValue;
                }
            }
        }

        // Iterate over the array list items and print a histogram.
        int nBarLength;
        String strFormat = "%n %" + nMaxCaptionLength + "s";
        for (int nArrayIdx = 0; nArrayIdx < strCaptions.size();  nArrayIdx++) {
            System.out.printf(strFormat,strCaptions.get(nArrayIdx));
            System.out.print(" ");

            // Prorate the length of the bars based on the maximum input value
            nBarLength = (int)((nValues.get(nArrayIdx)/(double)nMaxValue) * (double)nMAX_BAR_LENGTH);
            for (int nI = 0; nI < nBarLength; nI++) {
                System.out.print('*');
            }
        }

    }
}
