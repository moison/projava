package _02arrays;

/**
 * Created by John on 4/9/2016.
 * This program generates 20 random die tosses and brackets runs
 */
public class P6_12 {
    public static void main(String[] args) {

        final int nTOTAL_TOSSES = 20;
        final int nMIN_DIE_TOSS_VAL = 1;
        final int nMAX_DIE_TOSS_VAL = 6;
        int nDieTosses[] = new int[nTOTAL_TOSSES];


        // Generate random die tosses by multiplying math.random with the max possible value of dice and adding the integer portion to the min value of dice.
        for (int nI = 0; nI < nTOTAL_TOSSES; nI++) {
            nDieTosses[nI] = nMIN_DIE_TOSS_VAL + (int)(Math.random() * nMAX_DIE_TOSS_VAL);
        }

        // Using the pseudocode from the problem, check for run values.
        boolean inRun = false;
        for (int nI = 0; nI < nTOTAL_TOSSES; nI++) {
            if (inRun) {
                if (nDieTosses[nI] != nDieTosses[nI-1]) {
                    System.out.print(')');
                    inRun=false;
                }
                //Perform the below piece of code only till one less than the size of array to avoid index overflow error.
            } else if (nI < (nTOTAL_TOSSES - 1)){
                if (nDieTosses[nI] == nDieTosses[nI+1]) {
                    System.out.print('(');
                    inRun=true;
                }
            }
            System.out.print(nDieTosses[nI]);
        }
        if (inRun) {
            System.out.print(')');
        }
    }
}
