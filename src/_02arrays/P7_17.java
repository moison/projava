package _02arrays;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by John on 4/9/2016.
 * This program an input log file delimited by ';' and writes each entry to the respective output file based on the value in second input column.
 */
public class P7_17 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        // Get input and output file paths from user
        System.out.print("Please enter the complete path of the input file: ");
        String strInputFile = in.nextLine();
        System.out.print("Please enter the path for the output files: ");
        String strOutputFilePath = in.nextLine();

        // Try to process the file paths provided while catching IO Exception and File Not Found errors
        try {

            // Define a scanner to read the input file and printwriter to write to the output file
            Scanner inpFile = new Scanner(new File(strInputFile));

            // Define string array to hold split input values, variable to hold formatted output line and line counter to be used in the output line
            String strInputLine[];
            String strFileName,strOutputFile,strOutputLine;

            // Define array list to keep track of output files and also an array list of printwriter objects.
            ArrayList<String> strFileIndex = new ArrayList<>();
            ArrayList<PrintWriter> fileOutput = new ArrayList<>();

            while (inpFile.hasNext()) {

                // Split and capture various columns of input file.
                strInputLine = inpFile.nextLine().split(";");
                strFileName = strInputLine[1];
                strOutputLine = strInputLine[0] + ';' + strInputLine[2] + ';' + strInputLine[3];

                // Check if output filename already exists, if not, add entry and also create new printwriter object for it.
                if (!strFileIndex.contains(strFileName)) {
                    strOutputFile = strOutputFilePath + File.separator + strFileName + ".txt";
                    strFileIndex.add(strFileName);
                    fileOutput.add(new PrintWriter(strOutputFile));
                }

                // Get position of filename from array strFileIndex and use the index value to write to correct object of printwriter in fileOutput array list.
                fileOutput.get(strFileIndex.indexOf(strFileName)).println(strOutputLine);
            }

            // Iterate and close all files
            for (int nI = 0;nI < strFileIndex.size(); nI++) {
                fileOutput.get(nI).close();
            }

        } catch (IOException FileErr) {
            System.out.println(FileErr.getMessage());
        }

    }
}
