package _02arrays;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by John on 4/9/2016.
 * This program takes two sorted array lists and merges them.
 */
public class P6_28 {
    public static void main(String[] args) {

        // Define two sorted array lists
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(4,7,9,9,11));

        // Call method to merge the sorted array lists and print the result.
        System.out.println(mergeSorted(a,b));
    }

    // Method to accept two sorted integer array list and return the merge-sorted array list.
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {

        // Define integer array list to hold the result set to be returned.
        ArrayList<Integer> mergeSortedAL = new ArrayList<Integer>();

        // Define variables to hold size of array lists and index counters.
        int nAIdx = 0;
        int nBIdx = 0;
        int nASize = a.size();
        int nBSize = b.size();

        // Perform loop till both counters have reached the size of the array lists.
        while (nAIdx < nASize || nBIdx < nBSize) {

            // Perform compare if only both index counters are within the size of the array lists.
            if (nAIdx < nASize & nBIdx < nBSize) {
                //Compare and add the lower value to the result array list to ensure sorted order. Increment the index counter of the value being added.
                if (a.get(nAIdx) <= b.get(nBIdx)) {
                    mergeSortedAL.add(a.get(nAIdx));
                    nAIdx++;
                } else {
                    mergeSortedAL.add(b.get(nBIdx));
                    nBIdx++;
                }
                // if only one input array list is to be processed( i.e. lesser than size of array list), add its value to the result and increment index counter.
            } else if (nAIdx < nASize) {
                mergeSortedAL.add(a.get(nAIdx));
                nAIdx++;
            } else {
                mergeSortedAL.add(b.get(nBIdx));
                nBIdx++;
            }
        }
        // Return the merge sorted array list back to the caller.
        return mergeSortedAL;
    }
}
