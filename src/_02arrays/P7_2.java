package _02arrays;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;

/**
 * Created by John on 4/8/2016.
 * This program reads a file containing text and writes the output to another file with line numbers
 */
public class P7_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        // Get input and output file paths from user
        System.out.print("Please enter the complete path of the input file: ");
        String strInputFile = in.nextLine();
        System.out.print("Please enter the complete path of the output file: ");
        String strOutputFile = in.nextLine();

        // Try to process the file paths provided while catching IO Exception and File Not Found errors
        try {
            // Define a scanner to read the input file and printwriter to write to the output file
            Scanner inpFile = new Scanner(new File(strInputFile));
            PrintWriter outFile = new PrintWriter(strOutputFile);

            // Define string variable to hold formatted output line and line counter to be used in the output line.
            String strOutputLine;
            int nInputLineCount=1;

            while (inpFile.hasNext()) {
                // Format output line by adding line number. Write to output and increment line counter.
                strOutputLine = "/* " + nInputLineCount + " */ " + inpFile.nextLine();
                outFile.println(strOutputLine);
                nInputLineCount++;
            }
            // Close output file at the end.
            outFile.close();

            // Catch IO Exception error and print the error message.
        } catch (IOException FileErr) {
            System.out.println(FileErr.getMessage());
        }
    }
}
