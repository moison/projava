package _02arrays;

import java.util.Scanner;

/**
 * Created by John on 4/9/2016.
 * This program accepts a 5 digit postal zip code and converts it into a barcode.
 */

public class P5_25 {
    public static void main(String[] args) {

        // Define scanner and accept zip code from console
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a zip code: ");
        int nZipCode = in.nextInt();

        // Call method to print barcode by passing the zip code.
        printBarCode(nZipCode);
    }

    // Method to print barcode
    public static void printBarCode(int nZipCode) {


        // Convert the integer zip code to string to iterate over each character
        String strZipCode = Integer.toString(nZipCode);

        // Define variable to hold the current digit of the zip code being processed.
        int nCurrDigit;

        // Define variable to hold sum of digits of zip code to determine the check digit.
        int nSumDigits = 0;

        // Print the opening frame bar
        System.out.print('|');

        // Iterate over each digit of the zip code and call printDigit method to print the barcode for that digit.
        for (int nI=0; nI < strZipCode.length(); nI++) {
            nCurrDigit = Character.getNumericValue(strZipCode.charAt(nI));
            nSumDigits+=nCurrDigit;
            printDigit(nCurrDigit);
        }
        // Determine the check digit and determining its modulus with 10 and subtracting the result from 10.
        int nCheckDigit = 10 - nSumDigits%10;

        // Print out the bar equivalent of the check digit.
        printDigit(nCheckDigit);

        // Print the final frame bar
        System.out.print('|');
    }

    // Method to print barcode per digit
    public static void printDigit(int d) {
        // Define and initialize an array with table values provided in the problem definition.
        // The values have been re-arranged to be in 0,1,2..9 order to match the index of the array.
        // Performs look-up on corresponding index value from array and prints out by replacing 0 with : and 1 with |
        String strDigit2Bar[] = {"11000","00011","00101","00110","01001","01010","01100","10001","10010","10100"};
        System.out.print((strDigit2Bar[d].replace('0',':')).replace('1','|'));
    }
}
