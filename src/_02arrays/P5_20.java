package _02arrays;

import java.util.Scanner;

/**
 * Created by John on 4/8/2016.
 * This program tests whether the entered year is a leap year
 */
public class P5_20 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a year: ");
        int nYear = in.nextInt();

        //Call the isLeapYear method and test the boolean result.
        if (isLeapYear(nYear)) {
            System.out.println(nYear + " is a leap year.");
        } else {
            System.out.println(nYear + " is not a leap year.");
        }


    }

    // Method to test whether a leap year based on the rules provided in the problem
    public static boolean isLeapYear (int nYear) {

        boolean bLeapYearFlag;

        // A given year is a leap year if it is divisible by 4 and: i) it is not divisible by 100 or ii) divisible by 400
        if ((nYear % 4 == 0) & (nYear % 100 != 0 || nYear % 400 == 0)) {
            bLeapYearFlag = true;
        } else {
            bLeapYearFlag = false;
        }
        return  bLeapYearFlag;
    }
}
