package _02arrays;

import java.util.Scanner;

/**
 * Created by John on 4/8/2016.
 * This program accepts a sentence (list of words) and swaps two characters (except first and last character) and prints out each scrambled word.
 */
public class P5_8 {
    public static void main(String[] args) {

        // Define instance of Scanner to capture input from user.
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a sentence: ");

        // Split the input sentence into words since we are processing each word at a time.
        String strWords[] = in.nextLine().split(" ");
        for (String strWord : strWords) {
            System.out.print(charFlip(strWord) + " ");
        }

    }

    public static String charFlip (String strWord) {

            int nWordLen = strWord.length();

            //Ignore punctuation marks like .,;) by subtracting 1 from length of the word.
            int nLastCharPos = nWordLen -1;
            if (strWord.charAt(nLastCharPos) == '.' || strWord.charAt(nLastCharPos) == ',' || strWord.charAt(nLastCharPos) == ';' || strWord.charAt(nLastCharPos) == ')') {
                nWordLen-= 1;
            }

            // Since the first character should not be flipped, we start with a minimum index of 1, unless the first character is '(' in which case we will start from 2.
            int nMinPos;
            if (strWord.charAt(0) == '(' ) {
                nMinPos = 2;
            } else {
                nMinPos = 1;
            }

            // Define variable to hold the result to be returned.
            String strNewWord;

            // If the length of the word is less than or equal to 3 characters, there is nothing to change, hence, return the original word.
            if (nWordLen > 3 ) {
                // Calculate the position of a random character in the word while ensuring to add the offset for first character.
                // Use the Math.random function to arrive at a random character position.
                // The offset used is 3 since index is one less than the length, the last character needs to be ignored
                // and since the character to be swapped with will be to the right of the random position.
                int nRandomCharPos = nMinPos + (int)(Math.random() * (nWordLen-3));

                //Use substring to build the new word with flipped characters
                strNewWord = strWord.substring(0,nRandomCharPos) + strWord.charAt(nRandomCharPos+1) + strWord.charAt(nRandomCharPos) + strWord.substring(nRandomCharPos+2);
            } else {
                strNewWord = strWord;
            }

        return strNewWord;
    }
}


