package _04interfaces.P9_14;

/**
 * Measurable interface defines signature of method to getMeasure
 */
public interface Measurable {
    double getMeasure();
}
