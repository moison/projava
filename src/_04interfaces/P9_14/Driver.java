package _04interfaces.P9_14;

/**
 * Driver program for the class SodaCan.
 * This program creates a new object from the class SodaCan and invokes methods to calculate surface area and volume.
 */
public class Driver {
    public static void main(String[] args) {

        // Create an array of SodaCans while passing the height and radius.
        Measurable[] sodaCans = new Measurable[5];
        sodaCans[0] = new SodaCan(1,2);
        sodaCans[1] = new SodaCan(3,4);
        sodaCans[2] = new SodaCan(5,6);
        sodaCans[3] = new SodaCan(7,8);
        sodaCans[4] = new SodaCan(9,10);

        System.out.println("Average surface area of the array of soda cans is " + canAverage(sodaCans));

    }

    // Invoke methods with SodaCan class to calculate average surface area
    public static double canAverage(Measurable[] sodaCans) {

        if (sodaCans.length ==  0) {
            return 0.0;
        } else {
            double dTotalSurfaceArea = 0;
            for (Measurable sodaCan : sodaCans) {
                dTotalSurfaceArea += sodaCan.getMeasure();
            }
            return dTotalSurfaceArea / sodaCans.length;
        }

    }
}
