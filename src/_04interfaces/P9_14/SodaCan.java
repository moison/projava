package _04interfaces.P9_14;

/**
 * SodaCan class provides functionality to store properties of a sodacan and method to calculate surface area.
 */
public class SodaCan implements Measurable {

    // Declare member variables to store dimensions such as height and radius.
    private double mHeight;
    private double mRadius;

    // Constructor to initialize member variables with values being passed.
    public SodaCan (double nHeight, double nRadius) {
        mHeight = nHeight;
        mRadius = nRadius;
    }

    // Method to calculate surface area of the soda can using the formula for cylinder
    public double getMeasure() {
        return 2 * Math.PI * mRadius * mHeight + 2 * Math.PI * Math.pow(mRadius,2);
    }
}
