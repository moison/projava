package _04interfaces.P9_23;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Menu driven driver program for appointment book
 */
public class Driver {

    // Define arraylist to hold various types of appointments.
    private static ArrayList<Appointment> mApptList = new ArrayList<>();
    private static DateFormat datFormat = new SimpleDateFormat("MM/dd/yyyy");

    public static void main(String[] args) throws java.text.ParseException {

        int nOption;
        boolean bExit = false;
        Scanner in = new Scanner(System.in);

        // Display Main Menu and process selection.
        do {
            System.out.println("");
            System.out.println("       ***** Main Menu *****            ");
            System.out.println(" 1) Add new appointment.                ");
            System.out.println(" 2) Print appointments for a given date.");
            System.out.println(" 3) Save appointments to a file         ");
            System.out.println(" 4) Load appointments from a file       ");
            System.out.println(" 5) Exit                                ");
            System.out.print(  " Option: ");
            nOption = in.nextInt();

            switch (nOption) {
                case 1 :
                    addAppt();
                    break;
                case 2:
                    printAppt();
                    break;
                case 3:
                    save();
                    break;
                case 4:
                    load();
                    break;
                case 5:
                    System.out.println("Good bye!");
                    bExit = true;
                    break;
                default:
                    System.out.println("Invalid option. Try again!");
                    break;
            }

        } while (!bExit);
    }


    // Method to get inputs from console for adding a new appointment
    public static void addAppt () throws java.text.ParseException {
        String strApptType, strApptDesc;
        Date datApptDate;

        System.out.println(" Enter type of appointment (Onetime / Daily / Monthly): ");
        Scanner in = new Scanner(System.in);
        strApptType = in.nextLine();
        System.out.println(" Enter description for appointment: ");
        strApptDesc = in.nextLine();
        System.out.println(" Enter the date of appointment in (MM/DD/YYYY) format: ");
        datApptDate = datFormat.parse(in.nextLine());
        addApptCommon(strApptType,strApptDesc,datApptDate);
    }

    // Method to add a new appointment. Invoked when appointment is added via menu or loaded from file
    public static void addApptCommon (String strApptType, String strApptDesc, Date datApptDate) {
        switch (strApptType) {
            case "Onetime" :
                mApptList.add(new Onetime(strApptDesc,datApptDate));
                break;
            case "Daily" :
                mApptList.add(new Daily(strApptDesc,datApptDate));
                break;
            case "Monthly" :
                mApptList.add(new Monthly(strApptDesc,datApptDate));
                break;
            default:
                System.out.println("Invalid appointment type.");
                break;
        }
    }

    // Method to print appointments on a given date
    public static void printAppt () throws java.text.ParseException {
        Date datInpDate;
        boolean bApptFound = false;
        System.out.println(" Please enter a date to print appointments: ");
        Scanner in = new Scanner(System.in);
        datInpDate = datFormat.parse(in.nextLine());
        Calendar cal = Calendar.getInstance();
        cal.setTime(datInpDate);
        for (Appointment appt : mApptList) {
            if (appt.occursOn(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH))) {
                if (!bApptFound) {
                    System.out.println("Below appointments found:");
                    System.out.println("Type" + "\t" + "Description");
                    bApptFound=true;
                }
                System.out.println(appt.getApptType() + "\t" + appt.getApptDesc());
            }
        }
        if (!bApptFound) {
            System.out.println("No appointments found for the date :" + datInpDate);
        }
    }

    // Method to save all appointments to a specified file
    public static void save () {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter the complete path of the output file: ");
        String strOutputFile = in.nextLine();
        Calendar cal = Calendar.getInstance();

        try {
            PrintWriter outFile = new PrintWriter(strOutputFile);
            String strOutputLine;
            for (Appointment appt : mApptList) {
                cal.setTime(appt.getApptDate());
                strOutputLine = appt.getApptType() + "," + appt.getApptDesc() + "," + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DAY_OF_MONTH)
                        + "/" + cal.get(Calendar.YEAR);
                outFile.println(strOutputLine);
            }

            // Close output file at the end.
            outFile.close();
        } catch (IOException FileErr) {
            System.out.println(FileErr.getMessage());
        }
    }

    // Method to load all appointments from a specified file
    public static void load () throws java.text.ParseException {

        // Get input and output file paths from user
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter the complete path of the input file: ");
        String strInputFile = in.nextLine();

        try {
            Scanner inpFile = new Scanner(new File(strInputFile));
            mApptList.clear();
            String[] strInputLine;
            String[] strInputDate;
            int nYear, nMonth, nDay;
            Date datApptDate;

            while (inpFile.hasNext()) {

                // Split and capture various columns of input file.
                strInputLine = inpFile.nextLine().split(",");
                strInputDate = strInputLine[2].split("/");
                nMonth = Integer.parseInt(strInputDate[0]);
                nDay = Integer.parseInt(strInputDate[1]);
                nYear = Integer.parseInt(strInputDate[2]);
                datApptDate = datFormat.parse(nMonth + "/" + nDay + "/" + nYear);
                addApptCommon(strInputLine[0],strInputLine[1],datApptDate);
            }
        } catch (IOException FileErr) {
            System.out.println(FileErr.getMessage());
        }
    }

}