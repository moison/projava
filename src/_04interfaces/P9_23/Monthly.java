package _04interfaces.P9_23;

import java.util.Calendar;
import java.util.Date;

/**
 * Monthly class extends Appointment class.
 */
public class Monthly extends Appointment {

    // Invoke superclass constructor to initialize member variables
    public Monthly (String strDescription, Date datApptDate) {
        super(strDescription,datApptDate);
    }

    // Method to check if monthly appointment occurs on a given date
    public boolean occursOn(int year, int month, int day) {

        Calendar cal = Calendar.getInstance();

        // Access appointment date
        cal.setTime(getApptDate());

        // Since monthly appointments occur every month on a given day, return true if day matches.
        if (cal.get(Calendar.DAY_OF_MONTH) == day) {
            return true;
        } else {
            return false;
        }
    }

    // Method to return appointment type
    public String getApptType() {
        final String APPT_TYPE = "Monthly";
        return APPT_TYPE;
    }
}
