package _04interfaces.P9_23;

import java.util.Calendar;
import java.util.Date;

/**
 * Onetime class extends Appointment class.
 */
public class Onetime extends Appointment {

    // Invoke superclass constructor to initialize member variables
    public Onetime (String strDescription, Date datApptDate) {
        super(strDescription,datApptDate);
    }

    // Method to check if onetime appointment occurs on a given date
    public boolean occursOn(int year, int month, int day) {

        Calendar cal = Calendar.getInstance();

        // Access appointment date
        cal.setTime(super.getApptDate());

        // Return true if year, month and day match
        if (cal.get(Calendar.YEAR) == year && cal.get(Calendar.MONTH) == month && cal.get(Calendar.DAY_OF_MONTH) == day) {
            return true;
        } else {
            return false;
        }
    }

    // Method to return appointment type
    public String getApptType() {
        final String APPT_TYPE = "Onetime";
        return APPT_TYPE;
    }
}
