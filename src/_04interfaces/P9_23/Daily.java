package _04interfaces.P9_23;

import java.util.Date;

/**
 * Daily class extends Appointment class.
 */
public class Daily extends Appointment{

    // Invoke superclass constructor to initialize member variables
    public Daily (String strDescription, Date datApptDate) {
        super(strDescription,datApptDate);
    }

    // Return true since daily appointment occurs everyday.
    public boolean occursOn(int year, int month, int day) {
        return true;
    }

    // Method to return appointment type
    public String getApptType() {
        final String APPT_TYPE = "Daily";
        return APPT_TYPE;
    }
}
