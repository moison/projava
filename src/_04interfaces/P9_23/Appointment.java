package _04interfaces.P9_23;

import java.util.Date;

/**
 * Appointment abstract class
 */
public abstract class Appointment {

    // Define member variables to store description and appointment date
    private String mDescription;
    private Date mApptDate;

    // Constructor for appointment class to initialize member variables
    public Appointment(String strDescription, Date datApptDate) {
        this.mDescription = strDescription;
        this.mApptDate = datApptDate;
    }

    // Define abstract methods
    public abstract boolean occursOn(int year, int month, int day) ;
    public abstract String getApptType();

    // Define methods to access member variables
    public Date getApptDate() {
        return mApptDate;
    }
    public String getApptDesc() {
        return mDescription;
    }

}
