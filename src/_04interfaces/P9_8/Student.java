package _04interfaces.P9_8;

/**
 * Student class has major and extends Person class
 */
public class Student extends Person {

    // Declare member variable for Student class
    private String mMajor;

    // Constructor for Student class which invokes constructor of superclass
    public Student(String strName, int nYear, String strMajor) {
        super(strName, nYear);
        mMajor = strMajor;
    }

    // To string method to convert member variables into String type
    @Override
    public String toString() {
        return super.toString() + "[Major=" + mMajor + "]";
    }

}
