package _04interfaces.P9_8;

/**
 * Instructor class has salary and extends Person class
 */
public class Instructor extends Person {

    // Declare member variable for Instructor class
    private double mSalary;

    // Constructor for Instructor class which invokes constructor of superclass
    public Instructor(String strName, int nYear, double dSalary) {
        super(strName,nYear);
        this.mSalary = dSalary;
    }

    // To string method to convert member variables into String type
    @Override
    public String toString() {
        return super.toString() + "[Salary=" + mSalary + "]";
    }
}
