package _04interfaces.P9_8;

/**
 * Driver program for Person, Student and Instructor class
 */
public class Driver {
    public static void main(String[] args) {

        // Create one object of each class type
        Person perOne = new Person("John Doe",1975);
        Student stuOne = new Student("John Doe Jr",1990,"Computer Science");
        Instructor insOne = new Instructor("John Doe Sr",1960,50000.99);

        // Implicitly invoke the toString method using println
        System.out.println(perOne);
        System.out.println(stuOne);
        System.out.println(insOne);
    }
}
