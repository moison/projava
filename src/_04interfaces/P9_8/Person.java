package _04interfaces.P9_8;

/**
 * Implement a person class with name and year of birth.
 */
public class Person {

    // Declare member variables for Person class
    private String mName;
    private int mBirthYear;

    // Constructor to initialize name and year of birth
    public Person(String strName, int nBirthYear) {
        this.mName = strName;
        this.mBirthYear = nBirthYear;
    }

    // To string method to convert member variables into String type
    public String toString() {
        return getClass().getName() + "[Name=" + mName + "][BirthYear=" + mBirthYear + "]";
    }
}
