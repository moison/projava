package _04interfaces.P9_13;

/**
 * Class to create a point in the x,y co-ordinate system with a label and extends java.awt.Point
 */
public class LabeledPoint extends java.awt.Point {

    private String mLabel;

    // Constructor to initialize member variable and invoke super constructor
    public LabeledPoint(int x, int y, String label) {
        super(x,y);
        mLabel = label;
    }

    // toString method overrides and provides functionality to convert member variables to string datatype.
    @Override
    public String toString () {
        return super.toString() + "[Label=" + mLabel + "]";
    }

}
