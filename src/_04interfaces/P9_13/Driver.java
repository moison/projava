package _04interfaces.P9_13;

/**
 * Driver program for LabeledPoint class
 */
public class Driver {
    public static void main(String[] args) {

        // Create a new object of the LabeledPoint class
        LabeledPoint lptPoint1 = new LabeledPoint(1,2,"Point1");

        // Implicitly invoke the toString method using println
        System.out.println(lptPoint1);
    }
}
