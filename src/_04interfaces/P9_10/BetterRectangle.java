package _04interfaces.P9_10;

import java.awt.*;

/**
 * Class to extend standard Rectangle class and add functions to calculate perimeter and area of the rectangle
 */
public class BetterRectangle extends Rectangle {

    // Constructor will invoke the super class constructor
    public BetterRectangle(int nLocationX, int nLocationY, int nWidth, int nHeight) {
        super.setLocation(nLocationX, nLocationY);
        super.setSize(nWidth, nHeight);
    }


    // Method to calculate and return perimeter of a rectangle
    public double getPerimeter() {
        return 2 * (getWidth() + getHeight());
    }

    // Method to calculate and return area of a rectangle
    public double getArea() {
        return getWidth() * getHeight();
    }
}
