package _04interfaces.P9_10;

/**
 * Driver program to test the BetterRectangle class
 */
public class Driver {
    public static void main(String[] args) {

        // Create an object of BetterRectangle class
        BetterRectangle btrRec1 = new BetterRectangle(0,0,2,4);

        // Invoke method to calculate and return perimeter
        System.out.println(btrRec1.getPerimeter());

        // Invoke method to calculate and return area
        System.out.println(btrRec1.getArea());
    }
}
