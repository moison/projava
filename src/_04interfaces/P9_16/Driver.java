package _04interfaces.P9_16;

/**
 * Driver program for the Country class
 */
public class Driver {
    public static void main(String[] args) {

        // Create an array of countries while passing the name and area.
        Measurable[] countries = new Measurable[5];
        countries[0] = new Country("Australia",2970000);
        countries[1] = new Country("India",1269000);
        countries[2] = new Country("China",3705000);
        countries[3] = new Country("Ireland",32595);
        countries[4] = new Country("Canada",3855000);

        System.out.println("Country with the largest area is " + ((Country)maximum(countries)).getName() );
    }

    // Method to determine country with the largest area
    public static Measurable maximum(Measurable[] objects) {

        // Assume first object is the largest
        int nMaxObjIdx = 0;

        // Iterate over each object in the array and find the largest object
        for (int nI = 0; nI < objects.length; nI++) {
            if (objects[nMaxObjIdx].getMeasure() < objects[nI].getMeasure()) {
                nMaxObjIdx = nI;
            }
        }

        // Return the largest object
        return objects[nMaxObjIdx];
    }
}
