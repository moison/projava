package _04interfaces.P9_16;

/**
 * Country class provides functionality to store country name and area, and methods to access the member variables.
 */
public class Country implements Measurable {

    // Define member variables
    private String mCountryName;
    private double mArea;

    // Constructor for Country class
    public Country(String strCountryName, double dArea) {
        this.mCountryName = strCountryName;
        this.mArea = dArea;
    }

    // Method to access area of the country
    public double getMeasure() {
        return mArea;
    }

    // Method to access name of the country
    public String getName() {
        return mCountryName;
    }
}
