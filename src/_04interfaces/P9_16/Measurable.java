package _04interfaces.P9_16;

/**
 * Measurable interface defines signature of method to getMeasure
 */
public interface Measurable {
    double getMeasure();
}
