package _01control;

/**
 * This program takes the month and day as input and prints the season to which the date belongs.
 * Created by moison on 4/2/2016.
 */
import java.util.Scanner;

public class P3_18 {
    public static void main(String[] args) {

        String strSeason = "";
        Scanner in = new Scanner(System.in);

        // Accept month and day from console
        System.out.print("Please enter a month (1-12): ");
        int nMonth = in.nextInt();
        System.out.print("Please enter a day (1-31): ");
        int nDay = in.nextInt();

        // Assign season based on the quarter of the year
        if (nMonth >= 1 & nMonth <=3) {
            strSeason="Winter";
        } else if (nMonth >= 4 & nMonth <=6) {
            strSeason="Spring";
        } else if (nMonth >= 7 & nMonth <=9) {
            strSeason="Summer";
        } else if (nMonth >= 10 & nMonth <=12) {
            strSeason="Fall";
        }

        // Adjust for season change on 21st of March, June, September, December
        if  (nMonth % 3 == 0 & nDay >= 21) {
            switch (strSeason) {
                case "Winter":  strSeason = "Spring";
                    break;
                case "Spring":  strSeason = "Summer";
                    break;
                case "Summer":  strSeason = "Fall";
                    break;
                default      :  strSeason = "Winter";
                    break;
            }
        }

        // Print the final result
        System.out.println("Season: " + strSeason);

    }
}

