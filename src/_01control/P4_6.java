package _01control;

/**
 * This program will print the minimum value from a set of inputs values
 * Created by johnmoison on 4/2/16.
 */

import java.util.Scanner;

public class P4_6 {
    public static void main(String[] args) {

        // Define boolean flag to check if first value is being processed
        boolean bFirst = true;
        int nInput,nMinValue = 0;

        // Accept input from console
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a sequence of integers (enter 'quit' to exit): ");

        // Loop while next input in scanner is an integer
        // Inside the loop, read from console. If first flag is true, assign input value as minimum.
        // For each consecutive input, test if the value is lower than the one in minimum value variable,
        // if yes, replace with value from input, else discard the input value.
        while(in.hasNextInt()) {
            nInput = in.nextInt();
            if (bFirst) {
                nMinValue = nInput;
                bFirst = false;
            } else if (nInput < nMinValue) {
                nMinValue = nInput;
            }
        }
        // Print out the minimum value.
        System.out.println("Minimum value: " + nMinValue);

    }
}
