package _01control;

/** Created by moison on 4/3/2016.
 *  This program takes an input integer and prints all the prime numbers up to that number
 *  The program defines an array for collecting new prime numbers.
 *  Each new candidate in the sequence is tested against the set of available primes.
 *  If the new sequence number is not divisible by any of the available primes, then the new number is also prime and will be added to the array.
 */
import java.util.Scanner;

public class P4_18 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //Accept input integer from user
        System.out.print("Please enter an integer: ");
        int nUserInput = in.nextInt();

        //Define an integer array of the size of user input integer since that would be maximum number of primes between 1 and the input number
        int nPrimeNumbers[] = new int[nUserInput];
        int nPrimeArrayIdx = 0;
        boolean bPrimeFlag;

        //Start loop from 2 since it is the lowest prime number
        for (int nI = 2; nI < nUserInput; nI++ ) {

            //Initially set the prime flag to true; If we find a successful divisor, the flag will be set to false and break out of the loop
            bPrimeFlag=true;
            for (int nJ  = 0; nJ < nPrimeArrayIdx; nJ++) {
                //If remainder is zero, it means the number is divisible, set primeflag to false
                if ( nI % nPrimeNumbers[nJ] == 0) {
                    bPrimeFlag=false;
                    break;
                }
            }

            // If primeflag is still true, it accept the number as a prime number
            if (bPrimeFlag) {
                nPrimeNumbers[nPrimeArrayIdx++] = nI;
            }
        }

        // Loop through the array and print out all the collected prime numbers
        for (int nK = 0; nK < nPrimeArrayIdx; nK++) {
            System.out.println(nPrimeNumbers[nK]);
        }
    }
}
