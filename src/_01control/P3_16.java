package _01control;

/**
 * This program reads 3 strings, sorts them lexicographically and prints the sorted result.
 * Created by moison on 4/2/2016.
 */
import java.util.Scanner;

public class P3_16 {
    public static void main(String[] args) {

        /** Define constant to hold number of input words as per problem definition
         *  This program can handle more than 3 words, hence, as per the requirement, we can modify the program
         *  to accept the number of words as input.
         */
        final int NUMBER_OF_WORDS = 3;

        // Define a hold variable of string datatype for swapping words
        String strWordHold;

        // Define an array to hold the input words. Length of array = number of input words
        String[] strInputWords = new String[NUMBER_OF_WORDS];

        Scanner in =  new Scanner(System.in);

        // Accept words from console
        System.out.print("Enter three strings: ");
        for(int i = 0; i < NUMBER_OF_WORDS; i++){
            strInputWords[i] = in.next();
        }

        /** Compare and swap based on the result of compareTo method
         *  For each consecutive iteration, the words to be compared will be decremented by 1
         *  since the lexicographically largest word would be placed in the last position in the previous loop.
         */
        for(int i = 1; i < NUMBER_OF_WORDS; i++){
            for(int j = 0; j < (NUMBER_OF_WORDS - i); j++) {
                if (strInputWords[j].compareTo(strInputWords[j+1]) > 0) {
                    strWordHold = strInputWords[j];
                    strInputWords[j] = strInputWords[j+1];
                    strInputWords[j+1] = strWordHold;
                }
            }
        }

        // Print the sorted result from the array
        for(int i = 0; i < NUMBER_OF_WORDS; i++){
            System.out.println(strInputWords[i]);
        }
    }
}

