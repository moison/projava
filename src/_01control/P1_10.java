package _01control;

/**
 * This program uses ASCII characters to draw an image in the output while ensuring to escape characters such as \ & "
 * Created by moison on 4/2/2016.
 */
public class P1_10 {
    public static void main(String[] args) {
        System.out.println("      __            /^\\");
        System.out.println("    .'  \\          / :.\\   ");
        System.out.println("   /     \\         | :: \\ ");
        System.out.println("  /   /.  \\       / ::: | ");
        System.out.println(" |    |::. \\     / :::'/  ");
        System.out.println(" |   / \\::. |   / :::'/");
        System.out.println(" `--`   \\'  `~~~ ':'/`          _ _ _ _ _ ");
        System.out.println("         /         (           /         \\");
        System.out.println("        /   0 _ 0   \\         /    I      \\ ");
        System.out.println("      \\/     \\_/     \\/  \t /    Love     \\");
        System.out.println("    -== '.'   |   '.' ==-   <  Programming |");
        System.out.println("      /\\    '-^-'    /\\      \\     in      /");
        System.out.println("        \\   _   _   /         \\   Java!   /");
        System.out.println("       .-`-((\\o/))-`-.         \\_ _ _ _ _/");
        System.out.println("  _   /     //^\\\\     \\   _    ");
        System.out.println(".\"o\".(    , .:::. ,    ).\"o\".  ");
        System.out.println("|o  o\\\\    \\:::::/    //o  o| ");
        System.out.println(" \\    \\\\   |:::::|   //    /   ");
        System.out.println("  \\    \\\\__/:::::\\__//    /   ");
        System.out.println("   \\ .:.\\  `':::'`  /.:. /      ");
        System.out.println("    \\':: |_       _| ::'/  ");
        System.out.println("     `---` `\"\"\"\"\"` `---`");
        System.out.println("");
        System.out.println(" Bunny image inspired from chris.com/ascii");
    }
}
