package _01control;

/**
 * This program takes input shorthand notation of playing cards and prints the full description
 * Created by moison on 4/2/2016.
 */
import java.util.Scanner;
public class P3_14
{
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        // Accept card notation from console
        System.out.print("Enter the card notation: ");
        String strNotation=in.next();

        // Define string variables to hold intermediate and final results
        String strRankNotatn,strRankDescrtn,strSuitNotatn,strSuitDescrtn;

        /** If first character notation is 1, then rank is 10, hence substring first two characters for rank of card
        *   else substring only the first character as rank.
        *   Suit is always 1 character, hence, the character of rank would be read as suit.
        */
        if (strNotation.charAt(0) == '1') {
            strRankNotatn = strNotation.substring(0,2);
            strSuitNotatn = strNotation.substring(2);
        }
        else {
            strRankNotatn = strNotation.substring(0,1);
            strSuitNotatn = strNotation.substring(1);
        }

        // Switch case on the rank notation to assign the appropriate description
        switch (strRankNotatn) {
            case "A":   strRankDescrtn = "Ace";
                break;
            case "2":   strRankDescrtn = "Two";
                break;
            case "3":   strRankDescrtn = "Three";
                break;
            case "4":   strRankDescrtn = "Four";
                break;
            case "5":   strRankDescrtn = "Five";
                break;
            case "6":   strRankDescrtn = "Six";
                break;
            case "7":   strRankDescrtn = "Seven";
                break;
            case "8":   strRankDescrtn = "Eight";
                break;
            case "9":   strRankDescrtn = "Nine";
                break;
            case "10":   strRankDescrtn = "Ten";
                break;
            case "J":   strRankDescrtn = "Joker";
                break;
            case "Q":   strRankDescrtn = "Queen";
                break;
            case "K":   strRankDescrtn = "King";
                break;
            default :   strRankDescrtn = "Invalid Rank";
                break;

        }

        // Switch case on the suit notation to assign the appropriate description
        switch (strSuitNotatn) {
            case "D":   strSuitDescrtn = "Diamonds";
                break;
            case "H":   strSuitDescrtn = "Hearts";
                break;
            case "S":   strSuitDescrtn = "Spades";
                break;
            case "C":   strSuitDescrtn = "Clubs";
                break;
            default :   strSuitDescrtn = "Invalid Rank";
                break;

        }

        // Print the description of rank and suit of card
        System.out.println(strRankDescrtn + " of " + strSuitDescrtn);
    }
}
