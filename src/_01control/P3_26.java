package _01control;

/** This program converts a decimal year to its equivalent roman numeral representation.
 * Created by moison on 4/2/2016.
 */
import java.util.Scanner;

public class P3_26 {
    public static void main(String[] args) {

        // Define & initialize a string variable to collect the various codes of roman numeral
        String strRomanNumeral = "";
        Scanner in = new Scanner(System.in);

        // Accept year input from console
        System.out.print("Please enter a positive integer to be converted to Roman numerals: ");
        int nInputNum = in.nextInt();

        // If year greater than 3999, print message in console and exit program
        if (nInputNum > 3999) {
            System.out.println("Please enter a value less than or equal to 3999.");
        }
        else {
            // Determine if year falls in a particular range.
            // If yes, accept the code and deduct that many years and iterate again till everything has been converted.
            while(nInputNum > 0) {
                if (nInputNum >= 1000) {
                    strRomanNumeral+="M";
                    nInputNum-=1000;
                } else if (nInputNum >= 900) {
                    strRomanNumeral+="CM";
                    nInputNum-=900;
                } else if (nInputNum >= 500) {
                    strRomanNumeral+="D";
                    nInputNum-=500;
                } else if (nInputNum >= 400) {
                    strRomanNumeral+="CD";
                    nInputNum-=400;
                } else if (nInputNum >= 100) {
                    strRomanNumeral+="C";
                    nInputNum-=100;
                } else if (nInputNum >= 90) {
                    strRomanNumeral+="XC";
                    nInputNum-=90;
                } else if (nInputNum >= 50) {
                    strRomanNumeral+="L";
                    nInputNum-=50;
                } else if (nInputNum >= 40) {
                    strRomanNumeral+="XL";
                    nInputNum-=40;
                } else if (nInputNum >= 10) {
                    strRomanNumeral+="X";
                    nInputNum-=10;
                } else if (nInputNum >= 9) {
                    strRomanNumeral+="IX";
                    nInputNum-=9;
                } else if (nInputNum >= 5) {
                    strRomanNumeral+="V";
                    nInputNum-=5;
                } else if (nInputNum >= 4) {
                    strRomanNumeral+="IV";
                    nInputNum-=4;
                } else if (nInputNum >= 1) {
                    strRomanNumeral+="I";
                    nInputNum-=1;
                }
            }
            // Print final result
            System.out.println("Roman Numeral: "+strRomanNumeral);
        }
    }
}

