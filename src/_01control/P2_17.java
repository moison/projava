package _01control;

/**
 * Created by johnmoison on 4/2/16.
 */
/**
 * This program calculates difference between two input times in 24 hour format and prints the difference.
 * Created by moison on 4/2/2016.
 */
import java.util.Scanner;

public class P2_17 {
    public static void main(String[] args) {

        // Define constants used in calculation
        final int MINUTES_IN_HOUR = 60;
        final int HOURS_IN_DAY = 24;

        Scanner in = new Scanner(System.in);

        // Accept time inputs from console
        System.out.print("Please enter the first time: ");
        int nFirstTime = in.nextInt();
        System.out.print("Please enter the second time: ");
        int nSecondTime = in.nextInt();

        // Extract hours and minutes portion from the input 24 hour time by dividing and modulus with 100
        int nFirstTimeHour = nFirstTime / 100;
        int nFirstTimeMinute = nFirstTime % 100;
        int nSecondTimeHour = nSecondTime / 100;
        int nSecondTimeMinute = nSecondTime % 100;

        int nDiffMinutes;

        // If first input time is lesser than second; perform simple substraction as both times are within the same day
        if (nFirstTime < nSecondTime) {
            nDiffMinutes = (nSecondTimeHour * MINUTES_IN_HOUR + nSecondTimeMinute)
                    - (nFirstTimeHour * MINUTES_IN_HOUR + nFirstTimeMinute);
        }
        // Else adjust for day change by adjusting the result.
        else {
            nDiffMinutes = (nFirstTimeHour * MINUTES_IN_HOUR + nFirstTimeMinute)
                    - (nSecondTimeHour * MINUTES_IN_HOUR + nSecondTimeMinute);

            nDiffMinutes = HOURS_IN_DAY * MINUTES_IN_HOUR - nDiffMinutes;
        }

        // Print the difference in minutes in 24 hour format by dividing and modulus with minutes in an hour (60)
        System.out.println(nDiffMinutes/MINUTES_IN_HOUR + " hours " + nDiffMinutes%MINUTES_IN_HOUR + " minutes");

    }
}

