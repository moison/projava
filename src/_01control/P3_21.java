package _01control;

/**
 * This program calculates the tax amount based upon the tax slabs defined for the year 1913
 * Created by moison on 4/2/2016.
 */
import java.util.Scanner;

public class P3_21 {
    public static void main(String[] args) {

        // Define constants to hold the variable tax income thresholds
        final double dFirstSlabAmt=50000;
        final double dSecondSlabAmt=75000;
        final double dThirdSlabAmt=100000;
        final double dFourthSlabAmt=250000;
        final double dFifthSlabAmt=500000;

        // Define constants to hold the corresponding income tax percentage
        final double dFirstSlabPrcnt=1;
        final double dSecondSlabPrcnt=2;
        final double dThirdSlabPrcnt=3;
        final double dFourthSlabPrcnt=4;
        final double dFifthSlabPrcnt=5;
        final double dSixthSlabPrcnt=6;

        // Accept income amount from console
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter your income amount: ");
        double dIncome = in.nextDouble();

        // Define variable to hold tax amount
        double dTax=0;

        // Define variable to hold taxable amount per slab
        double dCurrSlabTaxableAmt=0;

        /** Determine if the tax amount falls into a particular tax bracket
         *  If yes, extract the tax amount applicable in that range, and apply the tax percentage
         *  Also, deduct the taxed amount in current iteration from the total income amount
         */
        while(dIncome > 0) {
            if (dIncome > dFifthSlabAmt) {
                dCurrSlabTaxableAmt = dIncome - dFifthSlabAmt;
                dTax+= dCurrSlabTaxableAmt * dSixthSlabPrcnt/100 ;
            } else if (dIncome > dFourthSlabAmt & dIncome <= dFifthSlabAmt) {
                dCurrSlabTaxableAmt = dIncome - dFourthSlabAmt;
                dTax+= dCurrSlabTaxableAmt * dFifthSlabPrcnt/100 ;
            } else if (dIncome > dThirdSlabAmt & dIncome <= dFourthSlabAmt) {
                dCurrSlabTaxableAmt = dIncome - dThirdSlabAmt;
                dTax+= dCurrSlabTaxableAmt * dFourthSlabPrcnt/100 ;
            } else if (dIncome > dSecondSlabAmt & dIncome <= dThirdSlabAmt) {
                dCurrSlabTaxableAmt = dIncome - dSecondSlabAmt;
                dTax+= dCurrSlabTaxableAmt * dThirdSlabPrcnt/100 ;
            } else if (dIncome > dFirstSlabAmt & dIncome <= dSecondSlabAmt) {
                dCurrSlabTaxableAmt = dIncome - dFirstSlabAmt;
                dTax+= dCurrSlabTaxableAmt * dSecondSlabPrcnt/100 ;
            } else if (dIncome <= dFirstSlabAmt) {
                dCurrSlabTaxableAmt = dIncome;
                dTax+= dCurrSlabTaxableAmt * dFirstSlabPrcnt/100 ;
            }
            dIncome-= dCurrSlabTaxableAmt;
        }

        // Print out the formatted total tax amount for the income entered
        System.out.printf("Your total tax amount for the year 1913 is: $%.2f.%n", dTax);
    }
}


