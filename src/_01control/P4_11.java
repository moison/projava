package _01control;

/**
 * This program counts the number of syllables in a word. The last 'e' and vowels adjacent to another vowel are considered as one.
 * Created by John Moison on 4/3/2016.
 */
import java.util.Scanner;

public class P4_11 {
    public static void main(String[] args) {

        // Define and initialize variables
        int nIterLimit,nSyllableCount = 0;
        boolean bPrevCharIsVowel = false;

        // Define Scanner to read input data from console
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a word to count syllables: ");
        String strWord = in.next();

        // Determine upper limit for 'for' loop.
        // If last character is 'e', subtract 1 from length of string since 'e' in last position is to be ignored.
        if (Character.toLowerCase(strWord.charAt(strWord.length() - 1)) == 'e') {
            nIterLimit = strWord.length() - 1;
        } else {
            nIterLimit = strWord.length();
        }

        // Iterate over the characters in the string.
        // Perform case insensitive check to see the character is a vowel and standalone (not adjacent to another vowel).
        for (int nI = 0; nI < nIterLimit; nI++) {
            switch (Character.toLowerCase(strWord.charAt(nI))) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    if (!bPrevCharIsVowel) {
                        nSyllableCount++;
                        bPrevCharIsVowel = true;
                    }
                    break;
                default:
                    bPrevCharIsVowel = false;
                    break;
            }
        }

        // As per the problem definition, if syllable count is 0, set it to 1
        if (nSyllableCount == 0) {
            nSyllableCount++;
        }

        // Print final count value
        System.out.println("Syllable count: " + nSyllableCount);
    }
}
