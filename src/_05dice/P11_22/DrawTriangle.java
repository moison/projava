package _05dice.P11_22;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * This program provides an interactive way to draw a triangle
 * First click by the mouse draws a point, second click draws a line between point one and two, and third click completes a triangle.
 * Fourth click erases the previous triangle and is treated as first click i.e. it draws a point.
 */
public class DrawTriangle {

    // Define and instantiate member variables
    private JPanel mPanel = new JPanel();
    private int mX1, mX2, mX3, mY1, mY2, mY3, mStep;
    private int mClickCounter = 0;

    public static void main(String[] args) {

        // Declare final variables with frame width and height
        JFrame frame = new JFrame("Draw Triangle");
        frame.setContentPane(new DrawTriangle().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(600,400);
        frame.setVisible(true);
    }

    public DrawTriangle() {
        MousePressListener listener = new MousePressListener();
        mPanel.addMouseListener(listener);
    }

    // Class to implement mouse listener and take appropriate action on the frame
    //First click by the mouse draws a point, second click draws a line between point one and two, and third click completes a triangle.
    class MousePressListener extends MouseAdapter {
        public void mouseClicked (MouseEvent mouseEvent) {
            mStep = mClickCounter%3 + 1;

            switch (mStep) {
                case 1:
                    mPanel.removeAll();
                    mPanel.repaint();
                    mX1 = mouseEvent.getX();
                    mY1 = mouseEvent.getY();
                    mPanel.getGraphics().drawLine(mX1,mY1,mX1,mY1);
                    break;
                case 2:
                    mX2 = mouseEvent.getX();
                    mY2 = mouseEvent.getY();
                    mPanel.getGraphics().drawLine(mX1,mY1,mX2,mY2);
                    break;
                case 3:
                    mX3 = mouseEvent.getX();
                    mY3 = mouseEvent.getY();
                    mPanel.getGraphics().drawLine(mX2,mY2,mX3,mY3);
                    mPanel.getGraphics().drawLine(mX1,mY1,mX3,mY3);
                    break;
            }
            mClickCounter++;
        }
    }

}
