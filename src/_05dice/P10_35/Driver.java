package _05dice.P10_35;

import javax.swing.*;

/**
 * Driver program for BillFrame to create an app for restaurant bill.
 */
public class Driver {
    public static void main(String[] args) {

        // Create an instance of BillFrame class and display it
        JFrame frame = new BillFrame();
        frame.setTitle("Restaurant Bill Generator");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
