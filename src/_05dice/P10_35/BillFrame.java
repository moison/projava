package _05dice.P10_35;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;
import java.util.Hashtable;

/**
 * This class extends JFrame and provides layout of a restaurant bill generator application along with logic to calculate subtotal, tax and suggested tip.
 */
public class BillFrame extends JFrame {

    // Declare final variables with frame width and height
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 600;

    // Declare final variables for the rows and columns in the text area.
    private static final int AREA_ROWS = 10;
    private static final int AREA_COLUMNS = 50;

    private static final double TAX_PERCENT = 0.10; // 10% percent tax
    private static final double TIP_PERCENT = 0.15; // 15% percent tip

    // Define hash table to store predefined menu items with their price
    private static Hashtable<String,Double> htblPresetOrder;

    private JLabel itemDescLabel, itemPriceLabel, subTotalLabel, taxLabel, tipLabel, totalLabel;
    private JTextField itemDescField, itemPriceField;
    private JButton button;
    private JTextArea resultArea;
    private double dSubTotal, dTax, dTotal, dTip;

    public BillFrame() {
        htblPresetOrder = new Hashtable();
        createMasterPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    // Primary method for calling all others methods to build the main panel to be added to the frame.
    private void createMasterPanel() {
        JPanel presetOrderPanel = createPresetOrderPanel();
        JPanel customOrderPanel = createCustomOrderPanel();
        JPanel resultPanel = createResultPanel();

        JPanel masterPanel = new JPanel();
        masterPanel.setLayout(new BorderLayout());
        JPanel allOrderPanel = new JPanel();
        allOrderPanel.setLayout(new BorderLayout());
        allOrderPanel.add(presetOrderPanel,BorderLayout.NORTH);
        allOrderPanel.add(customOrderPanel,BorderLayout.CENTER);
        masterPanel.add(allOrderPanel,BorderLayout.NORTH);
        masterPanel.add(resultPanel,BorderLayout.CENTER);
        add(masterPanel);
    }

    // Method to create a panel with preset menu items.
    private JPanel createPresetOrderPanel() {
        JPanel orderPanel = new JPanel();
        orderPanel.setLayout(new GridLayout(2, 5));
        orderPanel.add(makeOrderButton("Caprese",9.99));
        orderPanel.add(makeOrderButton("Panzanella",9.99));
        orderPanel.add(makeOrderButton("Bruschetta",9.99));
        orderPanel.add(makeOrderButton("Focaccia",9.99));
        orderPanel.add(makeOrderButton("PastaCarbonara",9.99));
        orderPanel.add(makeOrderButton("Pizza",9.99));
        orderPanel.add(makeOrderButton("Risotto",9.99));
        orderPanel.add(makeOrderButton("PastaBasilico",9.99));
        orderPanel.add(makeOrderButton("Tiramisucake",9.99));
        orderPanel.add(makeOrderButton("PannaCoulis",9.99));
        return orderPanel;
    }

    // Method to provide option to add custom menu items with price amount.
    // This method calls other methods to build all components required for the custom order portion
    private JPanel createCustomOrderPanel() {
        JPanel orderPanel2 = new JPanel();
        createTextField();
        createButton();
        orderPanel2.add(itemDescLabel);
        orderPanel2.add(itemDescField);
        orderPanel2.add(itemPriceLabel);
        orderPanel2.add(itemPriceField);
        orderPanel2.add(button);
        JScrollPane scrollPane = new JScrollPane(resultArea);
        orderPanel2.add(scrollPane);
        return orderPanel2;
    }

    // Method to create text field area of custom orders.
    private void createTextField() {
        itemDescLabel = new JLabel(" Item Description: ");
        final int FIELD_WIDTH_DESC = 35;
        itemDescField = new JTextField(FIELD_WIDTH_DESC);
        itemDescField.setText("");

        itemPriceLabel = new JLabel(" Item Price: ");
        final int FIELD_WIDTH_PRICE = 5;
        itemPriceField = new JTextField(FIELD_WIDTH_PRICE);
        itemPriceField.setText("0.0");

    }

    // Define class to perform action of adding new custom menu item
    private class NewItemListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            processNewItem(itemDescField.getText(), Double.parseDouble(itemPriceField.getText()));
        }
    }

    // Method to add actionlistener class to the add item buton.
    private void createButton() {
        button = new JButton("Add Item");
        ActionListener listener = new NewItemListener();
        button.addActionListener(listener);
    }

    // Method to populate the result/display fields with newly added items.
    private JPanel createResultPanel() {
        JPanel resultPanel = new JPanel();
        resultArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
        resultArea.setText(" Item Name\t\t\tItem Price" + "\n\n");
        resultArea.setEditable(false);
        resultPanel.add(resultArea);

        subTotalLabel = new JLabel(" SubTotal: " + dSubTotal);
        resultPanel.add(subTotalLabel);

        taxLabel = new JLabel(" Tax: " + dTax);
        resultPanel.add(taxLabel);

        totalLabel = new JLabel(" Amount Due: " + dTotal);
        resultPanel.add(totalLabel);

        tipLabel = new JLabel(" Suggested tip 15%: " + dTip);
        resultPanel.add(tipLabel);

        return resultPanel;
    }

    // Class to implement action listener for preset menu items
    private class PresetOrderListener implements ActionListener {
        private String strItem;
        public PresetOrderListener(String strItem) {
             this.strItem = strItem;
        }

        public void actionPerformed(ActionEvent event) {
            processNewItem(strItem,htblPresetOrder.get(strItem));
        }
    }

    // Method to create a button for each preset menu item
    private JButton makeOrderButton (String strOrder, double dPrice) {
        htblPresetOrder.put(strOrder,dPrice);
        JButton button = new JButton(strOrder);
        ActionListener listener = new PresetOrderListener(strOrder);
        button.addActionListener(listener);
        return button;
    }

    // Common method to process the addition of new menu items
    private void processNewItem(String strItem, double dSelectedItemPrice) {
        resultArea.append(" " + strItem + "\t\t\t" + dSelectedItemPrice + "\n");
        dSubTotal += dSelectedItemPrice;
        dTax = Math.round(dSubTotal * TAX_PERCENT * 100)/100;
        dTotal = dSubTotal + dTax;
        dTip = Math.round(dTotal * TIP_PERCENT * 100)/100;

        subTotalLabel.setText(" SubTotal: " + dSubTotal);
        taxLabel.setText(" Tax: " + dTax);
        totalLabel.setText(" Amount Due: " + dTotal);
        tipLabel.setText(" Suggested tip 15%: " + dTip);
    }

}
