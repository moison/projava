package _05dice.pig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * This class creates a frame for the pig-dice game and has logic to determine the winner
 */
public class PigFrame extends JFrame {

    // Declare final variables with frame width and height
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 400;
    private final int MIN_DIE_TOSS_VAL = 1;
    private final int MAX_DIE_TOSS_VAL = 6;

    // Define variables to hold texts and scores
    private JLabel gameStatusLabel, userScoreLabel, computerScoreLabel;
    private int mUserScore, mUserTempScore, mComputerScore, mComputerTempScore, mCurrDiceVal;
    private JPanel computerPanel;
    private boolean bGameOver;

    public PigFrame(){
        createGamePanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    // Method to create overall layout of the game panel
    public void createGamePanel() {
        JPanel gamePanel = new JPanel();
        gamePanel.setLayout(new BorderLayout());

        JLabel gameLabel = new JLabel("Dice Game of Pig");
        gameLabel.setFont(new Font("Serif", Font.BOLD, 28));
        gameLabel.setHorizontalAlignment(JLabel.CENTER);
        gameStatusLabel = new JLabel(" Your Turn!");
        gameStatusLabel.setFont(new Font("Serif", Font.BOLD, 28));
        gameStatusLabel.setHorizontalAlignment(JLabel.CENTER);
        gamePanel.add(gameStatusLabel,BorderLayout.CENTER);
        gamePanel.add(gameLabel,BorderLayout.NORTH);
        gamePanel.add(createUserPanel(),BorderLayout.WEST);
        gamePanel.add(createComputerPanel(),BorderLayout.EAST);
        add(gamePanel);
    }

    // Method to capture the actions by the user (throw v/s hold)
    private class UserActionListener implements ActionListener {
        private String strButton;
        public UserActionListener(String strButton) {
            this.strButton = strButton;
        }
        public void actionPerformed(ActionEvent event) {
            playGame(strButton);
        }
    }

    // Method to create the user side of the panel
    private JPanel createUserPanel() {
        JPanel userPanel = new JPanel();
        userPanel.setLayout(new BorderLayout());
        userScoreLabel = new JLabel("Your Total Score: " + mUserScore + "  Your Temporary Score: " + mUserTempScore);
        JButton throwButton = createButton("Throw");
        JButton holdButton = createButton("Hold");
        userPanel.add(userScoreLabel,BorderLayout.NORTH);
        userPanel.add(throwButton,BorderLayout.CENTER);
        userPanel.add(holdButton,BorderLayout.SOUTH);
        return userPanel;
    }

    // Method to create user buttons and attach actionlisteners
    private JButton createButton(String strButton) {
        JButton button = new JButton(strButton);
        button.addActionListener(new UserActionListener(strButton));
        return button;
    }

    // Method to create the computer side of the panel
    private JPanel createComputerPanel() {
        computerPanel = new JPanel();
        computerPanel.setLayout(new BorderLayout());
        computerScoreLabel = new JLabel("Computer's Score: " + mComputerScore + "  Computer's Temporary Score: " + mComputerTempScore);
        computerPanel.add(computerScoreLabel,BorderLayout.NORTH);
        return computerPanel;
    }

    // Method to contain the logic of the game
    private void playGame(String strButton) {

        // Determine the button clicked by the user
        switch (strButton) {
            case "Throw":
                //If user decides to throw, then call method to return a die value.
                // If die value is 1, set temporary total to zero and pass control to computer
                mCurrDiceVal = rollDice();
                if (mCurrDiceVal == 1) {
                    mUserTempScore = 0;
                    gameStatusLabel.setText("You rolled 1. It's computer's turn");
                    setUserScore();
                    playComputerTurn();
                } else {
                    // If die value was non-zero, add the value to the temporary score and ask user for next action.
                    mUserTempScore += mCurrDiceVal;
                    gameStatusLabel.setText("You rolled " + mCurrDiceVal);
                    if (mUserScore + mUserTempScore >= 100) {
                        mUserScore += mUserTempScore;
                        mUserTempScore = 0;
                        setUserScore();
                        bGameOver = true;
                        gameStatusLabel.setText("YOU WON!");
                        break;
                    }
                    setUserScore();
                }
                break;
            // If user decides to hold, add temporary score to total score and pass control to computer to play it's turn.
            case "Hold":
                mUserScore += mUserTempScore;
                mUserTempScore = 0;
                setUserScore();
                gameStatusLabel.setText("It's computer's turn!");
                playComputerTurn();
                break;
        }
    }

    // Method to set the user's score on label to be displayed on screen
    private void setUserScore() {
        userScoreLabel.setText("Your Total Score: " + mUserScore + "  Your Temporary Score: " + mUserTempScore);
    }

    // Method to set the computer's score on label to be displayed on screen
    private void setComputerScore() {
        computerScoreLabel.setText("Computer's Score: " + mComputerScore + "  Computer's Temporary Score: " + mComputerTempScore);
    }

    // Method to return a random die value between 1 and 6
    private int rollDice() {
        return MIN_DIE_TOSS_VAL + (int)(Math.random() * MAX_DIE_TOSS_VAL);
    }

    // Method to play computer's turn
    private void playComputerTurn() {
        boolean bCanPlay = true;

        do {
            mCurrDiceVal = rollDice();
            if (mCurrDiceVal == 1) {
                mComputerTempScore = 0;
                bCanPlay = false;
                setComputerScore();
                gameStatusLabel.setText("Computer rolled 1. It's your turn");
            } else {
                mComputerTempScore += mCurrDiceVal;
                gameStatusLabel.setText("Computer rolled " + mCurrDiceVal);
                if (mComputerScore + mComputerTempScore >= 100) {
                    mComputerScore += mComputerTempScore;
                    mComputerTempScore = 0;
                    setComputerScore();
                    bGameOver = true;
                    gameStatusLabel.setText("YOU LOST!");
                    break;
                }
                setComputerScore();
            }
            if (bCanPlay) {
                bCanPlay = getRandomBoolean();
                if (!bCanPlay) {
                    mComputerScore += mComputerTempScore;
                    mComputerTempScore = 0;
                    setComputerScore();
                    gameStatusLabel.setText("Computer played it's turn and decided to hold. It's your turn!");
                }
            }
        } while(bCanPlay);
    }

    // Method to randomly determine if the computer was to throw or hold
    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
