package _05dice.pig;

import javax.swing.*;

/**
 * Driver program for the pig-dice game
 */
public class Driver {
    public static void main(String[] args) {

        // Create an instance of PigFrame and display it
        JFrame frame = new PigFrame();
        frame.setTitle("Pig Dice Game");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
