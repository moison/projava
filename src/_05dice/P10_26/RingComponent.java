package _05dice.P10_26;

import javax.swing.*;
import java.awt.*;

/**
 * This class extends JComponents and draws 2d ovals to resemble the Olympic rings.
 */
public class RingComponent extends JComponent {

    private static Graphics g;

    public void paintComponent(Graphics g) {
        this.g = g;

        // Invoke the drawRing method with various positions and colors.
        drawRing(40,40,"Blue");
        drawRing(80,40,"Black");
        drawRing(120,40,"Red");
        drawRing(60,60,"Yellow");
        drawRing(100,60,"Green");
    }


    // Method to determine draw a 2d circle(oval) for a given position and color.
    private void drawRing(int nX, int nY, String strColor) {

        final int HEIGHT = 40;
        final int WIDTH = 40;
        Graphics2D g2D = (Graphics2D) g;
        g2D.setStroke(new BasicStroke(2));

        switch (strColor) {
            case "Blue":
                g2D.setColor(Color.BLUE);
                break;
            case "Black":
                g2D.setColor(Color.BLACK);
                break;
            case "Red":
                g2D.setColor(Color.RED);
                break;
            case "Yellow":
                g2D.setColor(Color.YELLOW);
                break;
            case "Green":
                g2D.setColor(Color.GREEN);
                break;
        }
        g2D.drawOval(nX , nY, WIDTH, HEIGHT);
    }
}

