package _05dice.P10_26;
import javax.swing.*;
import java.awt.*;

/**
 * Driver program for the RingComponent class.
 */
public class Driver {
    public static void main(String[] args) {

        // Declare final variables with frame width and height
        final int FRAME_WIDTH = 220;
        final int FRAME_HEIGHT = 200;

        // Define new frame with background color and title.
        JFrame frame = new JFrame();
        frame.setBackground(Color.white);
        frame.setTitle("Olympic Rings");
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);

        // Add an instance of the RingComponent to the frame and display it
        JComponent ringComponent = new RingComponent();
        frame.add(ringComponent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
