package _05dice.P10_2;

import javax.swing.*;

/**
 * This program counts the number of times the button was clicked and displays it in the dialog box.
 */
public class Driver {
    public static void main(String[] args) {
        JFrame frame =  new ButtonFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
