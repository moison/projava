package _05dice.P10_2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This program counts the number of times the button was clicked and displays it in the dialog box.
 */
public class ButtonFrame extends JFrame {

    // Declare final variables with frame width and height
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 100;

    private JLabel labelResult;
    private int nClicks;

    // Define constructor
    public ButtonFrame () {
        nClicks = 0 ;
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    // Declare method to create components to be filled in the frame
    private void createComponents() {

        // Declare the button
        JButton button = new JButton("Click Me!");

        // Define anonymous class with actionperformed and add it to the button
        button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                nClicks++;
                // Set label text with new click value
                labelResult.setText("I was clicked " + nClicks + " times!");
            }
        });

        // Set initial text of the label
        labelResult = new JLabel("I was clicked " + nClicks + " times!");
        JPanel panel = new JPanel();
        panel.add(button);
        panel.add(labelResult);
        add(panel);
    }
}
