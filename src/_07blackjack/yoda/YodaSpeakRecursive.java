package _07blackjack.yoda;

import java.util.Scanner;

/**
 * Program to print sentence in reverse using recursive call
 */
public class YodaSpeakRecursive {
    public static void main(String[] args) {

        String strInputSentence;

        Scanner scnIn = new Scanner(System.in);
        System.out.print("Please enter your sentence: ");
        strInputSentence = scnIn.nextLine();
        System.out.println(reverseWords(strInputSentence));
    }

    private static String reverseWords(String strSentence){
        if (strSentence.lastIndexOf(" ") < 0) {
            return strSentence;
        } else {
            return strSentence.substring(strSentence.lastIndexOf(" ") + 1) + " " + reverseWords(strSentence.substring(0,strSentence.lastIndexOf(" ")));
        }
    }
}
