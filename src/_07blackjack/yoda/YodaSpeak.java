package _07blackjack.yoda;

import java.util.Scanner;

/**
 * Program to print sentence in reverse using iteration
 */
public class YodaSpeak {
    public static void main(String[] args) {

        String[] strInputSentence;

        Scanner scnIn = new Scanner(System.in);
        System.out.print("Please enter your sentence: ");
        strInputSentence = scnIn.nextLine().split(" ");
        for (int nI = (strInputSentence.length - 1); nI >= 0; nI-- ) {
            if (nI > 0) {
                System.out.print(strInputSentence[nI] + " ");
            } else {
                System.out.print(strInputSentence[nI]);
            }
        }
    }
}
