package _07blackjack.blackjack;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by John on 5/14/2016.
 */
public class Card {
    protected final static String CARD_IMAGES_PATH = System.getProperty("user.dir") + File.separator + "src" + File.separator + "_07blackjack" + File.separator +  "blackjack" + File.separator + "images";
    private String mCardName;
    private int mCardValue;
    private BufferedImage mCardImage;


    public Card(String strCardName) {
        mCardName = strCardName;
        try {
            switch (strCardName.charAt(1)) {
                case 'A':
                    mCardValue = 11;
                    break;
                case '2':
                    mCardValue = 2;
                    break;
                case '3':
                    mCardValue = 3;
                    break;
                case '4':
                    mCardValue = 4;
                    break;
                case '5':
                    mCardValue = 5;
                    break;
                case '6':
                    mCardValue = 6;
                    break;
                case '7':
                    mCardValue = 7;
                    break;
                case '8':
                    mCardValue = 8;
                    break;
                case '9':
                    mCardValue = 9;
                    break;
                case 'J':
                    mCardValue = 10;
                    break;
                case 'Q':
                    mCardValue = 10;
                    break;
                case 'K':
                    mCardValue = 10;
                    break;
            }
            mCardImage = ImageIO.read(new File(CARD_IMAGES_PATH + File.separator + strCardName + ".png"));
        } catch (java.io.IOException e) {
            System.out.println("File not found:" + CARD_IMAGES_PATH + strCardName + ".png");
        }

    }

    public String getCardName() {
        return mCardName;
    }

    public int getCardValue() {
        return mCardValue;
    }

    public BufferedImage getCardImage() {
        return mCardImage;
    }
}
