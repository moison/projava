package _07blackjack.blackjack;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * This class creates a frame for the blackjack game and has logic to determine the winner
 */
public class BlackJackFrame extends JFrame {

    // Declare final variables with frame width and height
    private static final int FRAME_WIDTH = 900;
    private static final int FRAME_HEIGHT = 700;
    private static final int CARD_WIDTH = 75;
    private static final int CARD_HEIGHT = 100;

    // Define variables to hold texts and scores
    private JLabel dealAmountLabel, userAmountLabel, computerScoreLabel;
    private JLabel[] userCardLabels = new JLabel[10];
    private JLabel[] compCardLabels = new JLabel[10];
    private int mDealAmount, mUserAmount = 1000;
    private JPanel computerPanel, userPanel, userPanel1, userPanel11, userPanel12, userPanel2, userPanel3;
    private int mUserCardCount  = 0, mComputerCardCount = 0, mUserCardsValue = 0, mComputerCardsValue = 0, mUserAceCount = 0, mComputerAceCount = 0;
    private boolean bRoundInProgress = false;
    private Shoe mShoe;
    private ArrayList<Card> mUserCards, mComputerCards;


    // Constructor
    BlackJackFrame() {
        mShoe = new Shoe();
        mUserCards = new ArrayList<>();
        mComputerCards = new ArrayList<>();
        createGamePanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    // Method to create overall layout of the game panel
    private void createGamePanel() {
        JPanel gamePanel = new JPanel();
        gamePanel.setLayout(new BorderLayout());
        JLabel gameLabel = new JLabel("BlackJack");
        gameLabel.setFont(new Font("Serif", Font.BOLD, 28));
        gameLabel.setHorizontalAlignment(JLabel.CENTER);
        gamePanel.add(gameLabel,BorderLayout.NORTH);
        gamePanel.add(createComputerPanel(),BorderLayout.CENTER);
        gamePanel.add(createUserPanel(),BorderLayout.SOUTH);
        add(gamePanel);
    }

    // Method to create the computer side of the panel
    private JPanel createComputerPanel() {
        computerPanel = new JPanel();
        for (int nI = 0; nI < compCardLabels.length; nI++) {
            compCardLabels[nI] = new JLabel("");
            computerPanel.add(compCardLabels[nI]);
        }
        return computerPanel;
    }

    // Method to create the user side of the panel
    private JPanel createUserPanel() {
        userPanel = new JPanel();
        userPanel.setLayout(new BorderLayout());
        userPanel1 = new JPanel();
        userPanel11 = new JPanel();
        userPanel12 = new JPanel();
        userPanel12.setLayout(new BorderLayout());

        userPanel11.setLayout(new FlowLayout());
        for (int nI = 0; nI < userCardLabels.length; nI++) {
            userCardLabels[nI] = new JLabel("");
            userPanel11.add(userCardLabels[nI]);
        }


        dealAmountLabel = new JLabel(" Deal Amount: $" + mDealAmount);
        dealAmountLabel.setFont(new Font("Serif", Font.BOLD, 28));
        userAmountLabel = new JLabel("User Amount: $" + mUserAmount);
        userAmountLabel.setFont(new Font("Serif", Font.BOLD, 28));

        userPanel12.add(dealAmountLabel,BorderLayout.EAST);
        userPanel12.add(userAmountLabel,BorderLayout.WEST);


        userPanel1.add(userPanel11,BorderLayout.NORTH);
        userPanel1.add(userPanel12,BorderLayout.CENTER);

        userPanel2 = new JPanel();
        userPanel2.setLayout(new GridLayout(1,5));
        JButton oneDollarButton = createButton("$1");
        userPanel2.add(oneDollarButton);
        JButton fiveDollarButton = createButton("$5");
        userPanel2.add(fiveDollarButton);
        JButton twentyfiveDollarButton = createButton("$25");
        userPanel2.add(twentyfiveDollarButton);
        JButton hundredDollarButton = createButton("$100");
        userPanel2.add(hundredDollarButton);
        JButton fivehundredDollarButton = createButton("$500");
        userPanel2.add(fivehundredDollarButton);



        userPanel3 = new JPanel();
        userPanel3.setLayout(new GridLayout(2,2));

        JButton dealButton = createButton("Deal");
        userPanel3.add(dealButton);

        JButton clearButton = createButton("Clear");
        userPanel3.add(clearButton);

        JButton standButton = createButton("Stand");
        userPanel3.add(standButton);

        JButton hitButton = createButton("Hit");
        userPanel3.add(hitButton);

        userPanel.add(userPanel1,BorderLayout.NORTH);
        userPanel.add(userPanel2,BorderLayout.CENTER);
        userPanel.add(userPanel3,BorderLayout.SOUTH);

        return userPanel;
    }

    // Method to create user buttons and attach action listeners
    private JButton createButton(String strButton) {
        JButton button = new JButton(strButton);
        button.addActionListener(new UserActionListener(strButton));
        return button;
    }

    // Method to capture the actions by the user (stand v/s hit)
    private class UserActionListener implements ActionListener {
        private String strButton;
        public UserActionListener(String strButton) {
            this.strButton = strButton;
        }
        public void actionPerformed(ActionEvent event) {
            actionButton(strButton);
        }
    }

    private void actionButton(String strButton) {
        // Determine the button clicked by the user
        switch (strButton) {
            case "$1":
                validateAddAmount(1);
                break;
            case "$5":
                validateAddAmount(5);
                break;
            case "$25":
                validateAddAmount(25);
                break;
            case "$100":
                validateAddAmount(100);
                break;
            case "$500":
                validateAddAmount(500);
                break;
            case "Deal":
                if (mDealAmount == 0) {
                    JOptionPane.showMessageDialog(null,"Please add some coins.");
                } else {
                    dealCards();
                }
                break;
            case "Hit":
                addUserCard();
                break;
            case "Stand":
                dealerPlay();
                break;
            case "Clear":
                clearCards();
                break;
        }
    }

    // Validate and add amount for coin buttons
    private void validateAddAmount(int nButtonAmount) {
        if (bRoundInProgress) {
            JOptionPane.showMessageDialog(null,"Money cannot be added while game round is in progress.");
        }
        else if (nButtonAmount + mDealAmount > 500) {
            JOptionPane.showMessageDialog(null,"Deal amount cannot be greater than $500.");
        }
        else if (nButtonAmount <= mUserAmount) {
            mUserAmount-= nButtonAmount;
            mDealAmount+= nButtonAmount;
            dealAmountLabel.setText(" Deal Amount: $" + mDealAmount);
            userAmountLabel.setText("User Amount: $" + mUserAmount);
        } else {
            JOptionPane.showMessageDialog(null,"Insufficient funds.");
        }
    }

    private void dealCards() {
        bRoundInProgress = true;
        // Draw two cards for user
        mUserCards.add(mShoe.getRandomCard());
        setUserCardImage();
        checkUserAce();
        mUserCardCount++;

        mUserCards.add(mShoe.getRandomCard());
        setUserCardImage();
        checkUserAce();

        // Draw two cards for computer
        mComputerCards.add(mShoe.getRandomCard());
        setCompCardImage();
        checkCompAce();
        mComputerCardCount++;

        mComputerCards.add(mShoe.getRandomCard());
        checkCompAce();

        BufferedImage backImage;
        try {
            backImage = ImageIO.read(new File(Card.CARD_IMAGES_PATH + File.separator + "Back_card.png"));
            compCardLabels[mComputerCardCount].setIcon(new ImageIcon(scaleImage(backImage,CARD_WIDTH,CARD_HEIGHT)));
        } catch (java.io.IOException e) {
            System.out.println(e.getStackTrace());
        }

        repaint();

        //drawCards(userPanel1.getGraphics());
    }

    // Not used
    private void drawCards(Graphics g) {
        BufferedImage bufferedImage = scaleImage(mUserCards.get(0).getCardImage(),CARD_WIDTH,CARD_HEIGHT);
        g.drawImage(bufferedImage, 75, 100, null);
        repaint();
    }

    // Method taken from Adam's glab
    private static BufferedImage scaleImage(BufferedImage img, int nWidth, int nHeight) {

        BufferedImage newImage = new BufferedImage(nWidth, nHeight,
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = newImage.createGraphics();
        try {
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g.setBackground(new Color(0,0,0));
            g.clearRect(0, 0, nWidth, nHeight);
            g.drawImage(img, 0, 0, nWidth, nHeight, null);
        } finally {
            g.dispose();
        }
        return newImage;
    }

    // Add user card upon hit
    private void addUserCard() {
        mUserCards.add(mShoe.getRandomCard());
        mUserCardCount++;
        setUserCardImage();
        checkUserAce();
        if (checkUserBlackJack()) {
            userWon();
        } else if (checkUserBust()) {
            userLost();
        }
    }

    // Method for dealer play when user decides to stand
    private void dealerPlay() {
        // Show second card of dealer
        if (mComputerCardCount == 1) {
            compCardLabels[mComputerCardCount].setIcon(null);
            setCompCardImage();
            repaint();
            mComputerCardCount++;
        }

        boolean bRoundOver = false;
        boolean bCompTurnOver = false;

        do {
            if (checkCompBust() || (bCompTurnOver && mUserCardsValue >= mComputerCardsValue)) {
                JOptionPane.showMessageDialog(null,"You scored more than the dealer. You won!");
                userWon();
                bRoundOver = true;
            } else if (checkCompBlackJack() || mComputerCardsValue > mUserCardsValue) {
                JOptionPane.showMessageDialog(null,"Dealer scored more than you. You lost!");
                userLost();
                bRoundOver = true;
            }
            if (!bRoundOver && (mComputerCardsValue  - mComputerAceCount * 10) <= 17) { // Dealer must hit on soft 17
                mComputerCards.add(mShoe.getRandomCard());
                setCompCardImage();
                checkCompAce();
            } else {
                bCompTurnOver = true;
            }

        } while (!bRoundOver);
    }

    // Method to set user card image
    private void setUserCardImage() {
        userCardLabels[mUserCardCount].setIcon(new ImageIcon(scaleImage(mUserCards.get(mUserCardCount).getCardImage(),CARD_WIDTH,CARD_HEIGHT)));
    }

    // Method to set computer card image
    private void setCompCardImage() {
        compCardLabels[mComputerCardCount].setIcon(new ImageIcon(scaleImage(mComputerCards.get(mComputerCardCount).getCardImage(),CARD_WIDTH,CARD_HEIGHT)));
    }

    // Check if user hit black jack
    private boolean checkUserBlackJack() {
        if (mUserCardsValue == 21) {
            JOptionPane.showMessageDialog(null,"Congratulations. You've hit BlackJack.");
            return true;
        } else {
            return false;
        }
    }

    // Check if user bust
    private boolean checkUserBust() {
        if ( (mUserCardsValue  - mUserAceCount * 10) > 21 ) {
            JOptionPane.showMessageDialog(null,"Busted.");
            return true;
        } else {
            return false;
        }
    }

    // Check  if user has an Ace
    private void checkUserAce() {
        if (mUserCards.get(mUserCardCount).getCardValue() == 11) {
            mUserAceCount++;
        }
        mUserCardsValue += mUserCards.get(mUserCardCount).getCardValue();
    }


    // Set deal amount to zero if user lost
    private void userLost() {
        mDealAmount = 0;
        dealAmountLabel.setText(" Deal Amount: $" + mDealAmount);
        clearCards();
    }

    // Give the user money back if he won
    private void userWon() {
        mUserAmount = mUserAmount + 2 * mDealAmount;
        mDealAmount = 0;
        userAmountLabel.setText("User Amount: $" + mUserAmount);
        dealAmountLabel.setText(" Deal Amount: $" + mDealAmount);
        clearCards();
    }

    // Check if computer has ace
    private void checkCompAce() {
        if (mComputerCards.get(mComputerCardCount).getCardValue() == 11) {
            mComputerAceCount++;
        }
        mComputerCardsValue += mComputerCards.get(mComputerCardCount).getCardValue();
    }

    // Check if computer bust
    private boolean checkCompBust() {
        if ( (mComputerCardsValue  - mComputerAceCount * 10) > 21 ) {
            JOptionPane.showMessageDialog(null,"Dealer Bust. You won!");
            return true;
        } else {
            return false;
        }

    }

    // Check if computer hit black jack
    private boolean checkCompBlackJack() {
        if (mComputerCardsValue == 21) {
            JOptionPane.showMessageDialog(null,"Dealer hit blackjack. You lost.");
            return true;
        } else {
            return false;
        }
    }

    // Clear cards from screen for next round
    private void clearCards() {
        for (int nI = 0; nI < userCardLabels.length; nI++) {
            userCardLabels[nI].setIcon(null);
        }
        for (int nI = 0; nI < compCardLabels.length; nI++) {
            compCardLabels[nI].setIcon(null);
        }
        mComputerCards.clear();
        mUserCards.clear();
        mUserCardsValue = 0;
        mComputerCardsValue = 0;
        mUserCardCount = 0;
        mComputerCardCount = 0;
        bRoundInProgress = false;
    }

}
