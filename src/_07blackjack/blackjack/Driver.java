package _07blackjack.blackjack;

import javax.swing.*;
import java.awt.*;

/**
 * Driver program for blackjack game
 */
public class Driver {
    public static void main(String[] args) {
        JFrame blackJackFrame = new BlackJackFrame();
        blackJackFrame.setTitle("BlackJack");
        blackJackFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        blackJackFrame.getContentPane().setBackground(Color.green);
        blackJackFrame.setLocationRelativeTo(null);
        blackJackFrame.setVisible(true);
    }
}
