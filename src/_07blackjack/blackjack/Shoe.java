package _07blackjack.blackjack;

import java.util.HashMap;
import java.util.Random;

/**
 * Shoe class to hold deck of cards
 */
public class Shoe {


    private HashMap<String,Card> hashMapCards;
    private String[] mSuits = {"C","D","H","S"};
    private String[] mRanks = {"A","2","3","4","5","6","7","8","9","J","Q","K"};
    
    public Shoe() {
        hashMapCards = new HashMap<>();
        String strCardName;
        for (String strSuit : mSuits) {
            for (String strRank : mRanks) {
                strCardName = strSuit + strRank;
                hashMapCards.put(strCardName,new Card(strCardName));
            }
        }
    }

    // Method to generate random card
    public Card getRandomCard() {
        String strRandomCardKey = (String)hashMapCards.keySet().toArray()[new Random().nextInt(hashMapCards.keySet().toArray().length)];
        Card cardRandom = hashMapCards.get(strRandomCardKey);
        hashMapCards.remove(strRandomCardKey);
        return cardRandom;
    }
    
}
