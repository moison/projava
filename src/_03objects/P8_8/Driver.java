package _03objects.P8_8;

/**
 * Driver program for the Student and Grade class. It creates a new object of type Student, add few grades and prints out the current GPA.
 */
public class Driver {
    public static void main(String[] args) {

        // Create a new object called student1 which is instantiated from the Student class
        Student student1 = new Student("John Doe");

        // Add grades for the student.
        student1.addGrades("A+");
        student1.addGrades("B+");
        student1.addGrades("A-");

        // Print out the student's name and current GPA.
        System.out.println("Student: " + student1.getName());
        System.out.println("Current GPA: " + student1.getCurrGPA());
    }
}
