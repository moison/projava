package _03objects.P8_8;

/**
 * Class Grade to store grades with methods to convert string grades to numeric equivalents.
 */
public class Grade {
    private String mGradeStr;
    private double mGradeNbr;

    // Constructor to initialize member variables when a new object is created.
    public Grade(String strGrade) {
        mGradeStr = strGrade;
        mGradeNbr = ToNumeric(mGradeStr);
    }

    // Method to return numeric value of a grade stored in the member variable
    public double getNumericGrade () {
        return mGradeNbr;
    }

    // Static method used to convert string grade to numeric equivalent.
    public static double ToNumeric (String strGrade) {

        switch (strGrade) {
            case "A+":
                return 4.0;
            case "A":
                return 4.0;
            case "A-":
                return 3.7;
            case "B+":
                return 3.3;
            case "B":
                return 3.0;
            case "B-":
                return 2.7;
            case "C+":
                return 2.3;
            case "C":
                return 2.0;
            case "C-":
                return 1.7;
            case "D+":
                return 1.3;
            case "D":
                return 1.0;
            case "F":
                return 0.0;
            default:
                return 0.0;
        }
    }
}
