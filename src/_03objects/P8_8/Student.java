package _03objects.P8_8;

import java.util.ArrayList;

/**
 * This class simulates a student's progress card. It implements methods to add grades and calculate current GPA.
 */
public class Student {

    // Declare member variables to store name and list of grades.
    private String mName;
    private ArrayList<Grade> mGrades = new ArrayList<>();

    // Constructor to create a student object with only name.
    public Student (String strName) {
        mName = strName;
    }

    // Method to add grades. Each invocation adds a new object of type Grade to the array list.
    public void addGrades (String strGrade) {
        mGrades.add(new Grade(strGrade));
    }

    // When the below method is called to get current GPA, it iterates through the Grade objects stored in the array list,
    // retrieves the numeric value of the grade to determine a total and then calculate the average.
    public double getCurrGPA () {

        double dTotalGrade = 0;
        int nGradesCount = mGrades.size();

        for (int nI = 0; nI < nGradesCount; nI++) {
            dTotalGrade += mGrades.get(nI).getNumericGrade();
        }

        // Round the GPA to two decimal places.
        return Math.round(dTotalGrade/nGradesCount *  100.0) / 100.0;
    }

    public String getName () {
        return mName;
    }
}
