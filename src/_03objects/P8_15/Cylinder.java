package _03objects.P8_15;

/**
 * This class stores the properties of a cylinder and provides methods to calculate its volume and surface area.
 */
public class Cylinder {
    private double mRadius;
    private double mHeight;

    // Constructor to initialize member variables with parameters being passed.
    public Cylinder (double r, double h) {
        mRadius = r;
        mHeight = h;
    }

    // Calculate and return volume
    public double Volume () {
        return Math.PI * Math.pow(mRadius,2) * mHeight;
    }

    // Calculate and return surface area
    public double SurfaceArea () {
        return 2 * Math.PI * mRadius * mHeight + 2 * Math.PI * Math.pow(mRadius,2);
    }
}
