package _03objects.P8_15;

/**
 * This class stores the properties of a sphere and provides methods to calculate its volume and surface area.
 */
public class Sphere {
    private double mRadius;

    // Constructor to initialize member variables with parameters being passed.
    public Sphere (double r) {
        mRadius = r;
    }

    // Calculate and return volume
    public double Volume () {
        return 4/3.0 * Math.PI * Math.pow(mRadius,3);
    }

    // Calculate and return surface area
    public double SurfaceArea () {
        return 4 * Math.PI * Math.pow(mRadius,2);
    }
}
