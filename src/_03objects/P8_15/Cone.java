package _03objects.P8_15;

/**
 * This class stores the properties of a cone and provides methods to calculate its volume and surface area.
 */
public class Cone {
    private double mRadius;
    private double mHeight;

    // Constructor to initialize member variables with parameters being passed.
    public Cone (double r, double h) {
        mRadius = r;
        mHeight = h;
    }

    // Calculate and return volume
    public double Volume () {
        return Math.PI * Math.pow(mRadius,2) * mHeight/3;
    }

    // Calculate and return surface area
    public double SurfaceArea () {
        return Math.PI * mRadius * (mRadius + Math.sqrt(Math.pow(mHeight,2) + Math.pow(mRadius,2)));
    }
}
