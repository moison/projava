package _03objects.P8_15;

/**
 * Driver program which creates objects of class Sphere, Cylinder and Cone.
 * It first accepts the radius and height from console, and prints out the volume and surface area of various objects.
 * This approach is more object oriented than the implementation in P8_14.
 */
import java.util.InputMismatchException;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) throws InputMismatchException {

        // Declare variables to hold radius and height.
        double r,h;

        // Accept radius and height from console.
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter the value for radius: ");
        r = in.nextDouble();
        System.out.print("Please enter the value for height: ");
        h = in.nextDouble();

        // Create new objects of class Sphere, Cylinder and Cone.
        Sphere sphere1 = new Sphere(r);
        Cylinder cylinder1 = new Cylinder(r,h);
        Cone cone1 = new Cone(r,h);

        // Invoke methods to calculate volume and surface area of each object and print the results.
        System.out.println("");
        System.out.println("Volume of sphere: " + sphere1.Volume());
        System.out.println("Surface area of sphere: " + sphere1.SurfaceArea());
        System.out.println("Volume of cylinder: " + cylinder1.Volume());
        System.out.println("Surface area of cylinder: " + cylinder1.SurfaceArea());
        System.out.println("Volume of cone: " + cone1.Volume());
        System.out.println("Surface area of cone: " + cone1.SurfaceArea());

    }
}