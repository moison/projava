package _03objects.P8_11;

import java.util.ArrayList;

/**
 * This class simulates an assistant program for writing letters. It accepts information such sender, recipient and the line by line body of the letter.
 * It provides a method to print the entire letter.
 */
public class Letter {

    // Declare member variables to hold the sender, recipient and each line of the body.
    private String mFrom, mTo;
    private ArrayList<String> mBody = new ArrayList<>();

    // Constructor to instantiate an object with sender and recipient information.
    public Letter (String strFrom, String strTo) {
        mFrom = strFrom;
        mTo = strTo;
    }

    // Method to add a new line of words to the body of the letter.
    public void addLine (String strLine) {
        mBody.add(strLine);
    }

    // Method to print the entire letter.
    public String getText() {
        String strEntireLetter = "";
        strEntireLetter += "Dear " + mFrom + ":\n\n";
        for (int nI = 0; nI < mBody.size(); nI++) {
            strEntireLetter += mBody.get(nI) + "\n";
        }
        strEntireLetter += "\nSincerely,\n\n" + mTo;
        return strEntireLetter;
    }
}
