package _03objects.P8_11;

/**
 * Driver program for Letter class.
 * It creates a new object of class Letter with the sender and recipient.
 * Calls a method to add two lines to the body of the letter and finally prints the entire letter.
 */
public class Driver {
    public static void main(String[] args) {

        // Create a new object of class Letter called letter1
        Letter letter1 = new Letter("John","Mary");

        // Call addLine method to add two lines to the body of the letter.
        letter1.addLine("I am sorry we must part.");
        letter1.addLine("I wish you all the best.");

        // Method to print the entire letter.
        System.out.println(letter1.getText());
    }
}
