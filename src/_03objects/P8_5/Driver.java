package _03objects.P8_5;

/**
 * Driver program for the class SodaCan.
 * This program creates a new object from the class SodaCan and invokes methods to calculate surface area and volume.
 */
public class Driver {
    public static void main(String[] args) {

        // Create a new object of SodaCan while passing the height and radius.
        SodaCan Can1 = new SodaCan(3,5);

        // Invoke methods with SodaCan class to calculate surface area and volume.
        System.out.println("Surface area of can is " + Can1.getSurfaceArea());
        System.out.println("Volume of can is " + Can1.getVolume());
    }
}
