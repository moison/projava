package _03objects.P8_5;

/**
 * This class captures the dimensions of a soda can (cylinder) and provides methods to calculate surface area and volume.
 */
public class SodaCan {

    // Declare member variables to store dimensions such as height and radius.
    private double mHeight;
    private double mRadius;

    // Constructor to initialize member variables with values being passed.
    public SodaCan (double nHeight, double nRadius) {
        mHeight = nHeight;
        mRadius = nRadius;
    }

    // Method to calculate surface area of the soda can using the formula for cylinder
    public double getSurfaceArea() {
        return 2 * Math.PI * mRadius * mHeight + 2 * Math.PI * Math.pow(mRadius,2);
    }

    // Method to calculate volume of the soda can using the formula for cylinder
    public double getVolume() {
        return Math.PI * Math.pow(mRadius,2) * mHeight;
    }
}
