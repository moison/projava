package _03objects.P8_9;

/**
 * This class simulates the working of a combination lock.
 * It allows an instance of the class to be created with key combinations and provides methods to move the dial (left/right) and open the lock.
 */
public class ComboLock {

    // Declare private member variables to store the secret combination, attempt and success count.
    private int mSecret1, mSecret2, mSecret3, mAttemptCount, mSuccessCount;

    // Constructor to create an object of class ComboLock with the secret keys.
    public ComboLock (int nSecret1, int nSecret2, int nSecret3) {
        mSecret1 = nSecret1;
        mSecret2 = nSecret2;
        mSecret3 = nSecret3;
        mAttemptCount = 0;
        mSuccessCount = 0;
    }

    // Method to reset the attempt count and success count member variables.
    public void reset() {
        mAttemptCount = 0;
        mSuccessCount = 0;
    }

    // Method to turn the dial left.
    // As per the working of the combination lock, this method should be invoked during the first or third turn by the correct ticks to gain a success count.
    public void turnLeft(int nTicks) {
        mAttemptCount++;
        if ((mAttemptCount == 1 && nTicks == mSecret1) || (mAttemptCount == 3 && nTicks == mSecret3)) {
            mSuccessCount++;
        }
    }

    // Method to turn the dial right. This method should be invoked only during the second turn by the correct ticks to gain a success count.
    public void turnRight(int nTicks) {
        mAttemptCount++;
        if (mAttemptCount == 2 && nTicks == mSecret2) {
            mSuccessCount++;
        }
    }

    // Three consecutive success counts within three attempts allows the lock to be opened using this method.
    public boolean open() {
        if (mAttemptCount == 3 && mSuccessCount == 3) {
            return true;
        } else {
            return false;
        }
    }

}
