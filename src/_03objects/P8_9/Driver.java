package _03objects.P8_9;

/**
 * Driver program for the class ComboLock.
 */
public class Driver {
    public static void main(String[] args) {

        // Create a lock with secret key combination of 1,2,3
        ComboLock lock1 = new ComboLock(1,2,3);

        // First try with incorrect combination
        lock1.turnLeft(3);
        lock1.turnRight(2);
        lock1.turnLeft(1);
        if (lock1.open()) {
            System.out.println("Lock opened!");
        } else {
            System.out.println("Please reset and try again.");
        }

        // Reset dial
        lock1.reset();

        // Try with correct combination this time.
        lock1.turnLeft(1);
        lock1.turnRight(2);
        lock1.turnLeft(3);
        if (lock1.open()) {
            System.out.println("Lock opened!");
        } else {
            System.out.println("Please reset and try again.");
        }

    }
}
