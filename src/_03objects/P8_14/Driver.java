package _03objects.P8_14;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Driver program for the class Geometry.
 * The program accepts values of radius and height from the console.
 * Calculates the volume and surface area using the static method from class Geometry, and prints the results.
 */
public class Driver {
    public static void main(String[] args) throws InputMismatchException{

        // Define variables to hold values of radius and height.
        double r,h;
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter the value for radius: ");
        r = in.nextDouble();
        System.out.print("Please enter the value for height: ");
        h = in.nextDouble();

        // Call static methods of class Geometry and print the results.
        System.out.println("");
        System.out.println("Volume of sphere: " + Geometry.sphereVolume(r));
        System.out.println("Surface area of sphere: " + Geometry.sphereSurface(r));
        System.out.println("Volume of cylinder: " + Geometry.cylinderVolume(r,h));
        System.out.println("Surface area of cylinder: " + Geometry.cylinderSurface(r,h));
        System.out.println("Volume of cone: " + Geometry.coneVolume(r,h));
        System.out.println("Surface area of cone: " + Geometry.coneSurface(r,h));

    }
}
