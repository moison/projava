package _03objects.P8_14;

/**
 * This class provides static methods to calculate volume and surface area of sphere, cylinder and cone.
 */
public class Geometry {

    // Calculate and return the volume of sphere using the radius value being passed
    public static double sphereVolume(double r) {
        return 4/3.0 * Math.PI * Math.pow(r,3);
    }

    // Calculate and return the surface area of sphere using the radius value being passed
    public static double sphereSurface(double r) {
        return 4 * Math.PI * Math.pow(r,2);
    }

    // Calculate and return the volume of cylinder using the radius and height being passed
    public static double cylinderVolume(double r, double h) {
        return Math.PI * Math.pow(r,2) * h;
    }

    // Calculate and return the surface area of cylinder using the radius and height being passed
    public static double cylinderSurface(double r, double h) {
        return 2 * Math.PI * r * h + 2 * Math.PI * Math.pow(r,2);

    }

    // Calculate and return the volume of cone using the radius and height being passed
    public static double coneVolume(double r, double h) {
        return Math.PI * Math.pow(r,2) * h/3;
    }

    // Calculate and return the surface area of cone using the radius and height being passed
    public static double coneSurface(double r, double h) {
        return Math.PI * r * (r + Math.sqrt(Math.pow(h,2) + Math.pow(r,2)));
    }
}
