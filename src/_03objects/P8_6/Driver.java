package _03objects.P8_6;

/**
 * This driver program is to create an instance of the class Car and invoke methods to simulate adding gas, driving and checking fuel.
 */
public class Driver {
    public static void main(String[] args) {

        // Create new object of type Car with a fuel efficiency of 50 miles per gallon
        Car myHybrid = new Car(50);

        // Add 20 gallons of gas
        myHybrid.addGas(20);

        // Drive for 100 miles
        myHybrid.drive(100);

        // Check remaining gas in the fuel tank
        System.out.println(myHybrid.getGasLevel() + " gallons remaining in car's tank.");
    }
}
