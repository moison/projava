package _03objects.P8_6;

/**
 * This class stores the properties of a car and simulates driving of the car and implements methods to check fuel left in the gas tank.
 */

public class Car {

    // Declare member variables to hold fuel efficiency and fuel in tank.
    private double mFuelEfficiency;
    private double mFuelInTank;

    // Constructor class to instantiate an object of the class with fuel efficiency.
    public Car (double dFuelEfficiency) {
        mFuelEfficiency = dFuelEfficiency;
        mFuelInTank = 0;
    }

    // Method to add gas to the object of the Car class.
    public void addGas (double dGallons) {
        mFuelInTank += dGallons;
    }

    // Method to simulate driving of the car.
    // When the car is driven, we divide the miles driven by the fuel efficiency to determine the fuel spent, which is subtracted from the gas in fuel tank.
    public void drive (double dMiles) {
        mFuelInTank -= dMiles / mFuelEfficiency;
    }

    // Method to check the amount of gas left in fuel tank.
    public double getGasLevel () {
        return mFuelInTank;
    }
}
