package _03objects.P8_4;

/**
 * Driver program for the Address class. Instantiates two addresses, prints their contents and also invokes the compare method.
 */
public class Driver {
    public static void main(String[] args) {

        // Instantiate an object of Address class without apartment number
        Address addr1 = new Address(840, "S Canal St", "Chicago","IL", 60607);

        // Instantiate an object of Address class with apartment number
        Address addr2 = new Address(181, "W Madison St", 04, "Chicago","IL", 60603);

        // Print contents of first object
        addr1.print();

        // Print contents of second object
        addr2.print();

        // Invoke compare method and print out the result.
        if (addr2.comesBefore(addr1)) {
            System.out.println("Address 2 comes before address 1");
        } else {
            System.out.println("Address 2 does not come before address 1");
        }
    }
}
