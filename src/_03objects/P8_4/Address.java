package _03objects.P8_4;

/**
 * This class has stores a postal address with apartment number being optional, hence, two constructors are available
 * i.e. one with apartment number and the other without apartment number.
 * A print method prints the address and comesBefore method allows comparison of two addresses using their zipcodes.
 */
public class Address {
    private int mHouseNumber;
    private String mStreet;
    private int mApartmentNumber;
    private String mCity;
    private String mState;
    private int mPostalCode;

    // Constructor accepts the different portions of an address and assigns the values to member variables.
    // This constructor is invoked if apartment number is included in the address.
    public Address (int nHouseNumber, String strStreet, int nApartmentNumber, String strCity, String strState, int nPostalCode) {
        mHouseNumber = nHouseNumber;
        mStreet = strStreet;
        mApartmentNumber = nApartmentNumber;
        mCity = strCity;
        mState = strState;
        mPostalCode = nPostalCode;
    }
    // Constructor accepts the different portions of an address and assigns the values to member variables.
    // This constructor is invoked if apartment number is not included in the address.
    public Address (int nHouseNumber, String strStreet, String strCity, String strState, int nPostalCode) {
        mHouseNumber = nHouseNumber;
        mStreet = strStreet;
        mCity = strCity;
        mState = strState;
        mPostalCode = nPostalCode;
    }

    // Print method enables printing of the address.
    public void print () {
        System.out.println("Address:");
        if (mApartmentNumber != 0) {
            System.out.println(mStreet + ", " + mHouseNumber + ", " + mApartmentNumber + ",");
        } else {
            System.out.println(mStreet + ", " + mHouseNumber + ",");
        }
        System.out.println(mCity + ", " + mState + " - " + mPostalCode + "\n");
    }

    // Method to compare two addresses using their zip code.
    public boolean comesBefore (Address other) {
        if (this.mPostalCode < other.mPostalCode) {
            return true;
        } else {
            return false;
        }
    }
}
